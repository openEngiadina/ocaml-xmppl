# ocaml-xmppl

A reactive XMPP library for OCaml.

Goals:

- Be usable from Unix and Web browsers via [js_of_ocaml](https://ocsigen.org/js_of_ocaml/)
- Minimal XMPP features that allow more complex features to be built on top (e.g. library can be used to send/receive Iq stanzas, general Pub/Sub)
- Expose XMPP stanzas as reactive events (using the [React](https://erratique.ch/software/react) library).

# Status

This software should be considered to be alpha quality.

# Overview

ocaml-xmpp is split into following parts:

- a core XMPP library ([`xmppl`](src/xmppl)).
- transports ([`xmppl_unix`](src/transport/xmppl_unix) and [`xmppl_websocket`](src/transport/xmppl_websocket)) 
- helpers and combinators for working with XML ([`xmlc`](src/xmlc) and [`xmlt`](src/xmlt))
- partial implementations of various XMPP extensions (in multiple [`xmppl.*`](src/xep) sub-packages).

# Documentation

You can build the documentation locally with `dune build @doc` or view them [online](https://inqlab.net/projects/ocaml-xmpp/).

# Examples

Examples can be build with `dune build @examples`:

- `echobot_web`: An XMPP echobot that runs in the browser
- `xmppq`: A Unix tool to run various XMPP queries and commands

# License

[AGPL-3.0-or-later](./LICENSES/AGPL-3.0-or-later.txt)
