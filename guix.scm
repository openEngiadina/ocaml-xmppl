(use-modules
 (guix packages)
 (guix download)
 (guix git-download)
 (guix build-system dune)
 (guix build-system ocaml)
 ((guix licenses) #:prefix license:)
 (gnu packages license)
 (gnu packages ocaml)
 (gnu packages pkg-config))

(define-public ocaml-note
  (package
    (name "ocaml-note")
    (version "0.0.2")
    (source
     (origin
       (method url-fetch)
       (uri "https://erratique.ch/software/note/releases/note-0.0.2.tbz")
       (sha256
        (base32
         "09la36kpb3hcfyhgkhr1j3b6l3g4jy9b0ps3qbm04pfll1qmfzkg"))))
    (build-system ocaml-build-system)
    (arguments `(#:build-flags (list "build" "--tests" "true")
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'configure))))
    (native-inputs
     `(("ocaml-findlib" ,ocaml-findlib)
       ("ocamlbuild" ,ocamlbuild)
       ("ocaml-topkg" ,ocaml-topkg)
       ("opam" ,opam)))
    (home-page "https://erratique.ch/software/note")
    (synopsis
     "Declarative events and signals for OCaml")
    (description
     "
Note is an OCaml library for functional reactive programming (FRP). It
provides support to program with time varying values: declarative
events and signals.

Note is distributed under the ISC license.
")
    (license license:isc)))

(define-public ocaml-brr
  (package
    (name "ocaml-brr")
    (version "0.0.3")
    (source
      (origin
        (method url-fetch)
        (uri
	 (string-append
	  "https://erratique.ch/software/brr/releases/brr-" version
	  ".tbz"))
	(sha256
          (base32
            "0vmh3imq18yybmc3h24wr0z28ql187nwps3kq4n9nsxc7fip3kgl"))))
    (build-system ocaml-build-system)
    (arguments `(#:build-flags (list "build" "--tests" "true")
                 #:phases
                 (modify-phases %standard-phases
                   (delete 'configure))))
    (propagated-inputs
     `(("js-of-ocaml" ,js-of-ocaml)
       ("ocaml-note" ,ocaml-note)))
    (native-inputs
      `(("ocaml-findlib" ,ocaml-findlib)
        ("ocamlbuild" ,ocamlbuild)
        ("ocaml-topkg" ,ocaml-topkg)
        ("opam" ,opam)))
    (home-page "https://erratique.ch/software/brr")
    (synopsis "Browser programming toolkit for OCaml")
    (description
      "Brr is a toolkit for programming browsers in OCaml with the
[`js_of_ocaml`][jsoo] compiler. It provides:

* Interfaces to a selection of browser APIs.
* Note based reactive support (optional and experimental).
* An OCaml console developer tool for live interaction
  with programs running in web pages.
* A JavaScript FFI for idiomatic OCaml programming.

Brr is distributed under the ISC license. It depends on [Note][note]
and on the `js_of_ocaml` compiler and runtime – but not on its\nlibraries or syntax extension.

[note]: https://erratique.ch/software/note
[jsoo]: https://ocsigen.org/js_of_ocaml
")
    (license license:isc)))

(define-public ocaml-lwt-ssl
  (package
    (name "ocaml-lwt-ssl")
    (version "1.1.3")
    (home-page "https://github.com/ocsigen/lwt_ssl")
    (source
     (origin
       (method git-fetch)
       (uri (git-reference
             (url home-page)
             (commit version)))
       (file-name (git-file-name name version))
       (sha256
        (base32
         "0v417ch5zn0yknj156awa5mrq3mal08pbrvsyribbn63ix6f9y3p"))))
    (build-system dune-build-system)
    (arguments `(#:test-target "."))
    (propagated-inputs
     `(("ocaml-lwt" ,ocaml-lwt)
       ("ocaml-ssl" ,ocaml-ssl)))
    (properties `((upstream-name . "lwt_ssl")))
    (synopsis "OpenSSL binding for OCaml with concurrent I/O")
    (description "This OCaml library provides an Lwt-enabled wrapper around
@code{ocaml-ssl}, that performs I/O concurrently.")
    (license license:lgpl2.1+))) ; with linking exception

(define-public ocaml-logs
  (package
    (name "ocaml-logs")
    (version "0.7.0")
    (source (origin
              (method url-fetch)
              (uri (string-append "http://erratique.ch/software/logs/releases/"
                                  "logs-" version ".tbz"))
              (sha256
                (base32
                  "1jnmd675wmsmdwyb5mx5b0ac66g4c6gpv5s4mrx2j6pb0wla1x46"))))
    (build-system ocaml-build-system)
    (arguments
     `(#:tests? #f
       #:build-flags (list "build" "--with-js_of_ocaml" "true")
       #:phases
       (modify-phases %standard-phases
         (delete 'configure))))
    (native-inputs
     `(("ocamlbuild" ,ocamlbuild)
       ("opam" ,opam)))
    (propagated-inputs
     `(("fmt" ,ocaml-fmt)
       ("lwt" ,ocaml-lwt)
       ("mtime" ,ocaml-mtime)
       ("result" ,ocaml-result)
       ("cmdliner" ,ocaml-cmdliner)
       ("js-of-ocaml" ,js-of-ocaml)
       ("topkg" ,ocaml-topkg)))
    (home-page "https://erratique.ch/software/logs")
    (synopsis "Logging infrastructure for OCaml")
    (description "Logs provides a logging infrastructure for OCaml.  Logging is
performed on sources whose reporting level can be set independently.  Log
message report is decoupled from logging and is handled by a reporter.")
    (license license:isc)))

(define-public ocaml-markup-lwt
  (package
    (inherit ocaml-markup)
    (name "ocaml-markup-lwt")
    (arguments `(#:package "markup-lwt"))
    (propagated-inputs
     `(("ocaml-bisect-ppx" ,ocaml-bisect-ppx)
       ("ocaml-uchar" ,ocaml-uchar)
       ("ocaml-uutf" ,ocaml-uutf)
       ("ocaml-lwt" ,ocaml-lwt)
       ("ocaml-markup" ,ocaml-markup)))))

(define-public ocaml-alcotest-lwt
  (package
    (inherit ocaml-alcotest)
    (name "ocaml-alcotest-lwt")
    (arguments
     `(#:package "alcotest-lwt"
       #:test-target "."
       ;; TODO fix tests
       #:tests? #f))
    (propagated-inputs
     `(("ocaml-alcotest" ,ocaml-alcotest)
       ("ocaml-lwt" ,ocaml-lwt)
       ("ocaml-logs" ,ocaml-logs)))
    (native-inputs
     `(("ocaml-re" ,ocaml-re)
       ("ocaml-cmdliner" ,ocaml-cmdliner)))))

(define-public ocaml-xmpp
  (package
    (name "ocaml-xmpp")
    (version "0.0.0")
    (source #f)
    (build-system dune-build-system)
    (arguments '())
    (native-inputs
     `(("alcotest" ,ocaml-alcotest)
       ("alcotest-lwt" ,ocaml-alcotest-lwt)
       ("qcheck" ,ocaml-qcheck)
       ("reuse" ,reuse)))
    (propagated-inputs
     `(("ocaml-lwt" ,ocaml-lwt)
       ("ocaml-logs" ,ocaml-logs)
       ("ocaml-fmt" ,ocaml-fmt)
       ("ocaml-uunf" ,ocaml-uunf)
       ("ocaml-uuidm" ,ocaml-uuidm)
       ("ocaml-cmdliner" ,ocaml-cmdliner)
       ("ocaml-lwt-react" ,ocaml-lwt-react)
       ("ocaml-angstrom" ,ocaml-angstrom)
       ("ocaml-markup" ,ocaml-markup)
       ("ocaml-markup-lwt" ,ocaml-markup-lwt)
       ("ocaml-xmlm" ,ocaml-xmlm)
       ("ocaml-digestif" ,ocaml-digestif)
       ("ocaml-cstruct" ,ocaml-cstruct)
       ("ocaml-base64" ,ocaml-base64)
       ("ocaml-brr" ,ocaml-brr)
       ("ocaml-lwt-ssl" ,ocaml-lwt-ssl)
       ("js-of-ocaml" ,js-of-ocaml)))
    (home-page "https://gitlab.com/openengiadina/ocaml-xmpp")
    (synopsis #f)
    (description #f)
    (license license:agpl3+)))

ocaml-xmpp
