# This Makefile is only for publishing documentation. Use dune to
# build the OCaml library.

.PHONY: doc
doc:
	dune build @doc

.PHONY: publish-doc
publish-doc: doc
	rsync -rav -e ssh _build/default/_doc/_html/ qfwfq:/srv/http/inqlab.net-projects/ocaml-xmpp/ --delete

