(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

let of_markup_signals = function
  | `Start_element t -> Some (`El_start t)
  | `Text s -> Some (`Data (String.concat "" s))
  | `End_element -> Some `El_end
  | _ -> None

(* Parse a XML string to a stream of signals *)
let to_signals s =
  s |> Lwt_stream.of_string |> Markup_lwt.lwt_stream
  |> Markup_lwt.parse_xml ~report:(fun _location error ->
         fail_with @@ Markup.Error.to_string error)
  |> Markup.signals |> Markup_lwt.to_lwt_stream
  |> Lwt_stream.filter_map of_markup_signals

let parse parser s = Xmlc.Parser.parse_stream parser (to_signals s)

let make_test_case ?name ?expect parser s =
  Alcotest_lwt.test_case (Option.value ~default:s name) `Quick
    (fun _switch () ->
      let* result = Lwt_result.catch @@ parse parser s in
      match result with
      | Ok v' -> (
          match expect with
          | Some v ->
              if v = v' then return_unit else Alcotest.fail "not what expected"
          | None -> return_unit)
      | Error exn ->
          let* () = Lwt_io.printl (Printexc.to_string exn) in
          Alcotest.fail (Printexc.to_string exn))

let basic_combinators_tests =
  [ make_test_case ~name:"return" Xmlc.Parser.(return ()) "" ]

let xml_elements_tests =
  [
    make_test_case ~name:"start_element"
      Xmlc.Parser.(el_start ("", "hi"))
      "<hi>";
    make_test_case ~name:"start_element followed by end_element"
      Xmlc.Parser.(el_start ("", "hi") *> el_end)
      "<hi></hi>";
    make_test_case ~name:"element"
      Xmlc.Parser.(element ("", "a") (fun _ -> return ()))
      "<a/>";
    make_test_case ~name:"ignore_element" Xmlc.Parser.(ignore_element) "<a/>";
    make_test_case ~name:"ignore_element nested in many and choice"
      ~expect:[ "i"; "c"; "d"; "i"; "i"; "hello" ]
      Xmlc.Parser.(
        element ("", "a") (fun _ ->
            many
            @@ choice
                 [
                   element ("", "c") (fun _ -> return "c");
                   element ("", "d") (fun _ -> return "d");
                   (ignore_element >>| fun _ -> "i");
                   data;
                 ]))
      "<a><b/><c/><d/><b/><b/>hello</a>";
  ]

type complex_object = { id : string; name : string option }

let xml_complex_object_tests =
  let ns local = ("", local) in
  let complex_object_parser =
    Xmlc.Parser.(
      complex (ns "a") ~ignore_other:true
        ~required:[ ns "id" ]
        (fun _attributes -> return { id = ""; name = None })
        [
          element (ns "id") (fun _attributes ->
              data >>| fun id -> (ns "id", fun obj -> { obj with id }));
          element (ns "name") (fun _attributes ->
              data >>| fun name ->
              (ns "name", fun obj -> { obj with name = Some name }));
        ])
  in
  [
    make_test_case complex_object_parser ~expect:{ id = "42"; name = None }
      "<a><id>42</id></a>";
    (* parse the optional child element "name" *)
    make_test_case complex_object_parser
      ~expect:{ id = "43"; name = Some "Blups" }
      "<a><id>43</id><name>Blups</name></a>";
    (* invert order of child elements *)
    make_test_case complex_object_parser
      ~expect:{ id = "44"; name = Some "Blups" }
      "<a><name>Blups</name><id>44</id></a>";
    (* ignore any other child elements *)
    make_test_case complex_object_parser
      ~expect:{ id = "43"; name = Some "Blups" }
      "<a><id>43</id><name>Blups</name><this/><that>Ignored</that></a>";
  ]

let examples_tests =
  let parser_a = Xmlc.Parser.(element ("", "a") (fun _ -> return [])) in
  let parser_bn = Xmlc.Parser.(element ("", "bn") (fun _ -> data)) in
  let parser_b = Xmlc.Parser.(element ("", "b") (fun _ -> many parser_bn)) in
  let parser_c =
    Xmlc.Parser.(
      element ("", "c") (fun _ -> many @@ choice [ parser_a; parser_b ]))
  in
  let parser_d = Xmlc.Parser.(element ("", "d") (fun _ -> return [])) in
  [
    make_test_case parser_a ~expect:[] "<a></a>";
    make_test_case parser_a "<a/>";
    make_test_case parser_b ~expect:[] "<b/>";
    make_test_case parser_b ~expect:[ "Hi" ] "<b><bn>Hi</bn></b>";
    make_test_case ~expect:[ "Hello!"; "Hi!" ] parser_b
      "<b><bn>Hello!</bn><bn>Hi!</bn></b>";
    make_test_case
      ~expect:[ []; [ "Hello!"; "Hi!" ]; [] ]
      Xmlc.Parser.(parser_c >>= fun v -> parser_d *> return v)
      "<c><a/><b><bn>Hello!</bn><bn>Hi!</bn></b><a/></c><d/>";
  ]

(* let main =
 *   let parser =
 *     Xmlc.Parser.(
 *       element ("", "a") (fun _ ->
 *           many
 *           @@ choice
 *                [
 *                  element ("", "c") (fun _ -> return "c");
 *                  element ("", "d") (fun _ -> return "d");
 *                  (ignore_element >>| fun _ -> "i");
 *                  data;
 *                ]))
 *   in
 *   parse parser "<a><b/><c/><d/><b/><b/>hello</a>" >>= fun s ->
 *   Lwt_io.printf "hi: %s\n" (String.concat ", " s) >>= Lwt_io.flush_all *)

let () =
  Lwt_main.run
  @@ Alcotest_lwt.run ~verbose:true ~show_errors:true "Xmlc.Parser"
       [
         ("Basic combinators", basic_combinators_tests);
         ("XML elements", xml_elements_tests);
         ("Complex XML object", xml_complex_object_tests);
         ("Examples", examples_tests);
       ]
