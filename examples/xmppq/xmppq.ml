(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react
module Client = Xmppl_unix.Client
module Xmpp_roster = Xmppl_roster.Make (Client)

let src = Logs.Src.create "xmmpq"

module Log = (val Logs.src_log src : Logs.LOG)

module Options = struct
  open Cmdliner

  type t = {
    jid : Xmppl.Jid.t;
    password : string;
    disable_ssl : bool;
    port : int;
  }

  let jid_term =
    let doc = "Connect to server with JID" in
    Term.(
      const (fun s -> Option.get @@ Xmppl.Jid.of_string s)
      $ Arg.(
          required & opt (some string) None & info [ "jid" ] ~docv:"JID" ~doc))

  let password_term =
    let doc = "Password for authenticating with server" in
    Arg.(
      required
      & opt (some string) None
      & info [ "p"; "password" ] ~docv:"PASSWORD" ~doc)

  let disable_ssl_term =
    let doc = "Disable SSL" in
    Arg.(value & flag & info [ "disable-ssl" ] ~doc)

  let port_term =
    let doc = "XMPP server port to connect to." in
    Arg.(value & opt int 5223 & info [ "port" ] ~docv:"PORT" ~doc)

  let term =
    Term.(
      const (fun log_level style_renderer jid password disable_ssl port ->
          Logs.set_reporter (Logs_fmt.reporter ());
          Logs.set_level @@ log_level;
          Fmt_tty.setup_std_outputs ?style_renderer ();
          { jid; password; disable_ssl; port })
      $ Logs_cli.level () $ Fmt_cli.style_renderer () $ jid_term $ password_term
      $ disable_ssl_term $ port_term)
end

let connect (options : Options.t) =
  let* client =
    Client.create
      {
        port = options.port;
        disable_ssl = options.disable_ssl;
        incoming_signals =
          None
          (* Some (fun s -> Log.debug (fun m -> m "RECV SIGNAL: %a" Xmlm.pp_signal s)); *);
      }
      (* ~credentials:(`Anonymous "anon.strawberry.local") *)
      ~credentials:(`JidPassword (options.jid, options.password))
  in
  let jid_p = E.next @@ Client.on_connect client in
  let* () = Client.connect client in
  let* jid = jid_p in
  let () = Log.app (fun m -> m "Connected as %a." Xmppl.Jid.pp jid) in
  return client

let run t = Lwt_main.run (t >>= fun () -> Lwt_io.flush_all ())

module Xmpp_entity_capabilities = Xmppl_entity_capabilities.Make (Client)

let listen (options : Options.t) =
  let* client = connect options in

  let* entinty_capability_responder =
    Xmpp_entity_capabilities.advertise ~category:"client" ~type':"pc"
      ~name:"xmppq"
      [
        "http://jabber.org/protocol/caps";
        "http://jabber.org/protocol/disco#info";
        "http://jabber.org/protocol/disco#items";
        "urn:xmpp:microblog:0";
        "urn:xmpp:microblog:0+notify";
      ]
      client
  in

  (* Keep reference, so that entity capability requests are responded. *)
  E.keep entinty_capability_responder;

  let stanza_logger =
    Client.stanzas client
    |> E.map (fun stanza ->
           Log.app (fun m -> m "STANZA: %a" Xmppl.Stanza.pp stanza))
  in

  E.keep stanza_logger;

  (* Get roster. This will enable roster pushes. From RFC 6121:
   * if a resource wishes to receive both subscription
   * requests and roster pushes, it MUST both send initial presence and
   * request the roster. *)
  let* _roster = Xmpp_roster.get client in

  let state_logger =
    Client.state client
    |> S.map (fun state ->
           Log.app (fun m -> m "STATE: %a" Client.pp_state state))
  in

  S.keep state_logger;

  (* let* () = Client.send_xml client @@ Xmlc.Tree.make_element ("", "hey") in *)
  stanza_logger |> E.to_stream |> Lwt_stream.closed

let listen_cmd =
  let open Cmdliner in
  let doc = "Listen for incoming Stanzas and print" in
  Cmd.v (Cmd.info "listen" ~doc)
    Term.(const run $ (const listen $ Options.term))

module Ping = struct
  module Xmpp_ping = Xmppl_ping.Make (Client)

  let ping options =
    let* client = connect options in

    let rec loop () =
      (Xmpp_ping.ping client >|= fun () -> Log.app (fun m -> m "Pinged."))
      >>= fun () -> Lwt_unix.sleep 2.0 >>= loop
    in

    loop ()

  let ping_cmd =
    let open Cmdliner in
    let doc = "Ping server" in
    Cmd.v (Cmd.info "ping" ~doc) Term.(const run $ (const ping $ Options.term))
end

module Roster = struct
  let get_roster options =
    let* client = connect options in
    let* roster_items =
      Xmpp_roster.get client >|= Xmppl.Jid.Map.to_seq >|= Seq.map snd
    in
    return
    @@ Log.app (fun m -> m "%a" Fmt.(seq Xmpp_roster.Item.pp) roster_items)

  let get_roster_cmd =
    let open Cmdliner in
    let doc = "Get Roster" in
    Cmd.v
      (Cmd.info "roster_get" ~doc)
      Term.(const run $ (const get_roster $ Options.term))

  let roster_watch options =
    let* client = connect options in
    let* roster = Xmpp_roster.roster client in
    roster |> S.changes
    |> E.map (fun roster -> roster |> Xmppl.Jid.Map.to_seq |> Seq.map snd)
    |> E.map (fun roster ->
           Log.app (fun m -> m "%a" Fmt.(seq Xmpp_roster.Item.pp) roster))
    |> E.to_stream |> Lwt_stream.closed

  let roster_watch_cmd =
    let open Cmdliner in
    let doc = "Watch for roster changes" in
    Cmd.v
      (Cmd.info "roster_watch" ~doc)
      Term.(const run $ (const roster_watch $ Options.term))

  let roster_add options jid =
    let* client = connect options in
    let* result = Xmpp_roster.add_update client jid in
    return @@ Log.app (fun m -> m "%a" Xmppl.Stanza.Iq.pp_result result)

  let jid_term =
    let open Cmdliner in
    let doc = "JID of roster item to add" in
    Term.(
      const (fun s -> Option.get @@ Xmppl.Jid.of_string s)
      $ Arg.(required & pos 0 (some string) None & info [] ~docv:"JID" ~doc))

  let roster_add_cmd =
    let open Cmdliner in
    let doc = "Add Roster item" in
    Cmd.v
      (Cmd.info "roster_add" ~doc)
      Term.(const run $ (const roster_add $ Options.term $ jid_term))
end

module Echobot = struct
  let echobot options =
    let* client = connect options in
    let () = Log.app (fun m -> m "Starting Echobot") in

    (* Set presence by advertising capabilities *)
    let* entity_capabilities_responder =
      Xmpp_entity_capabilities.advertise ~category:"client" ~type':"console" []
        client
    in
    E.keep entity_capabilities_responder;

    let echobot_e =
      Client.stanzas client
      |> E.map_s (function
           | Xmppl.Stanza.Message ({ from = Some from; _ } as msg) ->
               let () =
                 Log.app (fun m ->
                     m "Received message from %a. Echoing." Xmppl.Jid.pp from)
               in
               Client.send_message client
                 { msg with to' = from; from = Some msg.to' }
           | _ -> return_unit)
    in

    (* Keep the GC from collecting the echobot *)
    E.keep echobot_e;

    echobot_e |> E.to_stream |> Lwt_stream.closed

  let echobot_cmd =
    let open Cmdliner in
    let doc = "Echobot" in
    Cmd.v (Cmd.info "echobot" ~doc)
      Term.(const run $ (const echobot $ Options.term))
end

module Disco = struct
  module Xmpp_disco = Xmppl_disco.Make (Client)

  let disco_info options to' =
    let* client = connect options in

    let () =
      Log.app (fun m -> m "Getting DISCO info for %a" Xmppl.Jid.pp to')
    in

    let* response =
      Xmpp_disco.query_info client ~to'
      (* ~node:"urn:xmpp:microblog:0" *)
    in

    let () =
      Log.app (fun m ->
          m "%a" Fmt.(list ~sep:Fmt.sp Xmpp_disco.pp_info_result) response)
    in
    Client.disconnect client

  let to_term =
    let open Cmdliner in
    let doc = "Send request to this identifier" in
    Term.(
      const (fun s -> Option.get @@ Xmppl.Jid.of_string s)
      $ Arg.(required & pos 0 (some string) None & info [] ~docv:"TO" ~doc))

  let disco_info_cmd =
    let open Cmdliner in
    let doc = "Get DISCO info" in
    Cmd.v
      (Cmd.info "disco_info" ~doc)
      Term.(const run $ (const disco_info $ Options.term $ to_term))

  let disco_items options to' =
    let* client = connect options in

    let () =
      Log.app (fun m -> m "Getting DISCO items for %a" Xmppl.Jid.pp to')
    in
    let* response =
      Xmpp_disco.query_items client ~to'
      (* ~node:"urn:xmpp:microblog:0" *)
    in

    let () =
      Log.app (fun m ->
          m "%a" Fmt.(list ~sep:Fmt.sp Xmpp_disco.Item.pp) response)
    in
    Client.disconnect client

  let disco_items_cmd =
    let open Cmdliner in
    let doc = "Get DISCO items" in
    Cmd.v
      (Cmd.info "disco_items" ~doc)
      Term.(const run $ (const disco_items $ Options.term $ to_term))
end

module PubSub = struct
  module Xmpp_pubsub = Xmppl_pubsub.Make (Client)

  let subscribe options service node =
    let* client = connect options in

    let () =
      Log.app (fun m ->
          m "Subscribing to node %s at %a..." node Xmppl.Jid.pp service)
    in

    let* _result = Xmpp_pubsub.subscribe ~to':service ~node client in

    return_unit

  let service_term =
    let open Cmdliner in
    let doc = "PubSub service" in
    Term.(
      const (fun s -> Option.get @@ Xmppl.Jid.of_string s)
      $ Arg.(required & pos 0 (some string) None & info [] ~docv:"SERVICE" ~doc))

  let node_term =
    let open Cmdliner in
    let doc = "PubSub node" in
    Arg.(required & pos 1 (some string) None & info [] ~docv:"NODE" ~doc)

  let subscribe_cmd =
    let open Cmdliner in
    let doc = "Subscribe to a PubSub node" in
    Cmd.v
      (Cmd.info "subscribe" ~doc)
      Term.(
        const run $ (const subscribe $ Options.term $ service_term $ node_term))

  let publish options service node =
    let* client = connect options in

    let atom_uri = "http://www.w3.org/2005/Atom" in
    let atom local = (atom_uri, local) in

    let item =
      Xmlc.Tree.(
        make_element
          ~children:
            [
              make_element
                ~attributes:
                  [ Xmlc.Namespace.default_xmlns_declaration atom_uri ]
                (atom "entry");
            ]
          (Xmpp_pubsub.Namespace.pubsub "item"))
    in

    Xmpp_pubsub.publish ~to':service ~node client (Some item) >|= fun _ -> ()

  let publish_cmd =
    let open Cmdliner in
    let doc = "Publish to a PubSub node" in
    Cmd.v (Cmd.info "publish" ~doc)
      Term.(
        const run $ (const publish $ Options.term $ service_term $ node_term))

  let retrieve options service node =
    let* client = connect options in

    let () =
      Log.app (fun m ->
          m "Subscribing to node %s at %a..." node Xmppl.Jid.pp service)
    in

    let* result = Xmpp_pubsub.retrieve ~to':service ~node client in

    return @@ Log.app (fun m -> m "%a" Xmppl.Stanza.Iq.pp_result result)

  let retrieve_cmd =
    let open Cmdliner in
    let doc = "Retrieve all items from PubSub node" in
    Cmd.v
      (Cmd.info "pubsub_retrieve" ~doc)
      Term.(
        const run $ (const retrieve $ Options.term $ service_term $ node_term))
end

module Mam = struct
  module Xmpp_mam = Xmppl_mam.Make (Client)

  let query options =
    let* client = connect options in
    Xmpp_mam.query client

  let query_cmd =
    let open Cmdliner in
    let doc = "Query the MAM archive" in
    Cmd.v
      (Cmd.info "mam_query" ~doc)
      Term.(const run $ (const query $ Options.term))
end

let cmds =
  [
    listen_cmd;
    Ping.ping_cmd;
    Roster.get_roster_cmd;
    Roster.roster_watch_cmd;
    Roster.roster_add_cmd;
    Echobot.echobot_cmd;
    PubSub.subscribe_cmd;
    PubSub.publish_cmd;
    PubSub.retrieve_cmd;
    Disco.disco_info_cmd;
    Disco.disco_items_cmd;
    Mam.query_cmd;
  ]

let main_cmd =
  let open Cmdliner in
  let doc = "xmppq - Tool for playing with XMPP" in
  let sdocs = Manpage.s_common_options in
  let exits = Cmd.Exit.defaults in
  let info = Cmd.info "xmppq" ~version:"0.0.0" ~doc ~sdocs ~exits in
  let default =
    Term.(ret (const (fun _ -> `Help (`Pager, None)) $ Options.term))
  in
  Cmd.group info ~default cmds

let () =
  let open Cmdliner in
  exit @@ Cmd.eval main_cmd
