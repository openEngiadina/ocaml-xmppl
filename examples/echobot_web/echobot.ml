(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react
open Js_of_ocaml_lwt
module Client = Xmppl_websocket.Client
open Brr

let src = Logs.Src.create "Echobot"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)

let echobot client =
  Client.stanzas client
  |> E.map_s (function
       | Xmppl.Stanza.Message ({ from = Some from; _ } as msg) ->
           let* () =
             Log.app (fun m ->
                 m "Received message from %a. Echoing." Xmppl.Jid.pp from)
           in
           Client.send_message client
             { msg with to' = from; from = Some msg.to' }
       | _ -> return_unit)

let main jid password =
  let opts : Client.transport_options =
    { ws_endpoint = Some "ws://localhost:5280/xmpp-websocket" }
  in

  let* client =
    Client.create (* Use hardcorded WebSocket endpoint *) opts
      ~credentials:(`JidPassword (jid, password))
  in
  let* () = Client.connect client in

  let presence =
    Xmlc.Tree.make_element
      ~attributes:
        [ Xmlc.Namespace.default_xmlns_declaration Xmppl.Namespace.client_uri ]
      (Xmppl.Namespace.client "presence")
  in

  let* () = Client.send_xml client presence in

  echobot client |> E.keep;

  return_unit

let () =
  Logs.set_reporter @@ Logs_browser.console_reporter ();
  Logs.set_level @@ Some Logs.Debug;
  ignore
  @@ main (Xmppl.Jid.of_string "user@strawberry.local" |> Option.get) "pencil"
