(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

module Make (Client : Xmppl.Client.S) = struct
  module Namespace = struct
    let pubsub_ns = "http://jabber.org/protocol/pubsub"
    let pubsub local = (pubsub_ns, local)
    let errors_ns = "http://jabber.org/protocol/pubsub#errors"
    let errors local = (errors_ns, local)
    let event_ns = "http://jabber.org/protocol/pubsub#event"
    let event local = (event_ns, local)
    let owner_ns = "http://jabber.org/protocol/pubsub#owner"
    let owner local = (owner_ns, local)
  end

  let create_node ~to' ?id client =
    let create_request =
      Xmlc.Tree.(
        make_element
          ~attributes:
            [ Xmlc.Namespace.default_xmlns_declaration Namespace.pubsub_ns ]
          ~children:
            [
              make_element
                ~attributes:
                  List.(
                    filter_map
                      (fun x -> x)
                      [
                        Option.map (fun id -> (Namespace.pubsub "node", id)) id;
                      ])
                (Namespace.pubsub "create");
            ]
          (Namespace.pubsub "pubsub"))
    in
    let* _response = Client.iq_set client ~to' create_request in
    return_unit

  let subscribe ~to' ~node client =
    let* jid = Client.jid client in
    let request =
      Xmlc.Tree.(
        make_element
          ~attributes:
            [ Xmlc.Namespace.default_xmlns_declaration Namespace.pubsub_ns ]
          ~children:
            [
              make_element
                ~attributes:
                  [
                    (Namespace.pubsub "node", node);
                    (Namespace.pubsub "jid", Xmppl.Jid.(to_string @@ bare jid));
                  ]
                (Namespace.pubsub "subscribe");
            ]
          (Namespace.pubsub "pubsub"))
    in
    Client.iq_set client ~to' request

  let publish ~to' ~node client item =
    let request =
      Xmlc.Tree.(
        make_element
          ~attributes:
            [ Xmlc.Namespace.default_xmlns_declaration Namespace.pubsub_ns ]
          ~children:
            [
              make_element
                ~attributes:[ (Namespace.pubsub "node", node) ]
                ~children:List.(filter_map (fun x -> x) [ item ])
                (Namespace.pubsub "publish");
            ]
          (Namespace.pubsub "pubsub"))
    in
    Client.iq_set client ~to' request

  let retrieve ~to' ~node client =
    let request =
      Xmlc.Tree.(
        make_element
          ~attributes:
            [ Xmlc.Namespace.default_xmlns_declaration Namespace.pubsub_ns ]
          ~children:
            [
              make_element
                ~attributes:[ (Namespace.pubsub "node", node) ]
                (Namespace.pubsub "items");
            ]
          (Namespace.pubsub "pubsub"))
    in
    Client.iq_get client ~to' request
end
