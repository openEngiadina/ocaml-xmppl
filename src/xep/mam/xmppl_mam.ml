(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt

module Make (Client : Xmppl.Client.S) = struct
  module Ns = struct
    let mam_uri = "urn:xmpp:mam:2"
    let mam local = (mam_uri, local)
  end

  let query client =
    (Client.iq_set client
    @@ Xmlc.Tree.(
         make_element
           ~attributes:[ Xmlc.Namespace.default_xmlns_declaration Ns.mam_uri ]
           (Ns.mam "query")))
    >|= fun _ -> ()
end
