(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt

module Make (Client : Xmppl.Client.S) = struct
  module Ns = struct
    let ping_uri = "urn:xmpp:ping"
    let ping local = ("urn:xmpp:ping", local)
  end

  let ping client =
    (Client.iq_get client
    @@ Xmlc.Tree.(
         make_element
           ~attributes:[ Xmlc.Namespace.default_xmlns_declaration Ns.ping_uri ]
           (Ns.ping "ping")))
    >|= fun _ -> ()
end
