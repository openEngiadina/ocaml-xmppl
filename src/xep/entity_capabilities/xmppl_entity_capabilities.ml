(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react

module Make (Client : Xmppl.Client.S) = struct
  module Ns = struct
    let caps_uri = "http://jabber.org/protocol/caps"
    let caps local = (caps_uri, local)
  end

  module Disco = Xmppl_disco.Make (Client)

  let generate_verification_string category type' lang name features =
    let sorted_features = List.sort String.compare features in
    let s =
      category ^ "/" ^ type' ^ "/"
      ^ Option.value ~default:"" lang
      ^ "/"
      ^ Option.value ~default:"" name
      ^ "<"
      ^ String.concat "<" sorted_features
      ^ "<"
    in
    Digestif.SHA1.(to_raw_string @@ digest_string s)
    |> Base64.encode_string ~pad:true

  let advertise ~category ~type' ?lang ?name
      ?(node = "https://inqlab.net/git/ocaml-xmpp.git") features client =
    (* Generate the verification string *)
    let verification_string =
      generate_verification_string category type' lang name features
    in

    (* The node to which Disco queries will be directed *)
    let query_node = node ^ "#" ^ verification_string in

    let query_response =
      Xmlc.Tree.(
        make_element
          ~attributes:
            [
              Xmlc.Namespace.default_xmlns_declaration Disco.Ns.disco_info_uri;
              (Disco.Ns.disco_info "node", query_node);
            ]
          ~children:
            List.(
              append
                [ Disco.Identity.(to_xml @@ create category type') ]
                (map Disco.Feature.to_xml features))
          (Disco.Ns.disco_info "query"))
    in

    (* Start listening and responding to disco requests *)
    let disco_responder =
      Disco.info_requests client query_node
      |> E.map_s (fun (get : Xmppl.Stanza.Iq.get) ->
             let result =
               Xmppl.Stanza.Iq.make_result ~id:get.id ?to':get.from
                 (Some query_response)
             in
             let* () = Client.send_xml client (Xmppl.Stanza.Iq.to_xml result) in
             return get.from)
    in

    let presence =
      Xmppl.Stanza.Presence.make
        [
          Xmlc.Tree.(
            make_element
              ~attributes:
                [
                  Xmlc.Namespace.default_xmlns_declaration Ns.caps_uri;
                  (Ns.caps "hash", "sha-1");
                  (Ns.caps "node", node);
                  (Ns.caps "ver", verification_string);
                ]
              (Ns.caps "c"));
        ]
    in

    let* () = Client.send_xml client (Xmppl.Stanza.Presence.to_xml presence) in

    return disco_responder
end
