(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** {1 Roster and Presence Subscription Management} *)

(** This module implements XMPP roster and subscription management as
specified in RFC 6121. *)

module type S = sig
  module Ns : sig
    val roster_uri : string
    val roster : string -> Xmlm.name
  end

  module Item : sig
    type t = {
      jid : Xmppl.Jid.t;
      approved : bool option;
      ask : string option;
      name : string option;
      subscription : string option;
      groups : string list;
    }

    val make :
      ?name:string ->
      ?groups:string list ->
      ?subscription:string ->
      ?approved:bool ->
      ?ask:string ->
      Xmppl.Jid.t ->
      t

    val pp : t Fmt.t [@@ocaml.toplevel_printer]
    val parser : t Xmlc.Parser.t
  end

  type client

  (** {1 Roster Management} *)

  type roster = Item.t Xmppl.Jid.Map.t

  val get : client -> roster Lwt.t
  val roster : client -> roster React.signal Lwt.t

  val add_update :
    client ->
    ?name:string ->
    ?groups:string list ->
    Xmppl.Jid.t ->
    Xmppl.Stanza.Iq.result Lwt.t

  val remove : client -> Xmppl.Jid.t -> Xmppl.Stanza.Iq.result Lwt.t

  (** {1 Managing Presence Subscription} *)

  val presence_subscribe : client -> Xmppl.Jid.t -> unit Lwt.t
  val presence_unsubscribe : client -> Xmppl.Jid.t -> unit Lwt.t
  val approve_presence_subscription : client -> Xmppl.Jid.t -> unit Lwt.t
  val deny_presence_subscription : client -> Xmppl.Jid.t -> unit Lwt.t
end

module Make (Client : Xmppl.Client.S) : S with type client = Client.t
