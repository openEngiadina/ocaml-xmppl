(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react

module type S = sig
  module Ns : sig
    val roster_uri : string
    val roster : string -> Xmlm.name
  end

  module Item : sig
    type t = {
      jid : Xmppl.Jid.t;
      approved : bool option;
      ask : string option;
      name : string option;
      subscription : string option;
      groups : string list;
    }

    val make :
      ?name:string ->
      ?groups:string list ->
      ?subscription:string ->
      ?approved:bool ->
      ?ask:string ->
      Xmppl.Jid.t ->
      t

    val pp : t Fmt.t [@@ocaml.toplevel_printer]
    val parser : t Xmlc.Parser.t
  end

  type client
  type roster = Item.t Xmppl.Jid.Map.t

  val get : client -> roster Lwt.t
  val roster : client -> roster React.signal Lwt.t

  val add_update :
    client ->
    ?name:string ->
    ?groups:string list ->
    Xmppl.Jid.t ->
    Xmppl.Stanza.Iq.result Lwt.t

  val remove : client -> Xmppl.Jid.t -> Xmppl.Stanza.Iq.result Lwt.t
  val presence_subscribe : client -> Xmppl.Jid.t -> unit Lwt.t
  val presence_unsubscribe : client -> Xmppl.Jid.t -> unit Lwt.t
  val approve_presence_subscription : client -> Xmppl.Jid.t -> unit Lwt.t
  val deny_presence_subscription : client -> Xmppl.Jid.t -> unit Lwt.t
end

module Make (Client : Xmppl.Client.S) = struct
  module Ns = struct
    let roster_uri = "jabber:iq:roster"
    let roster local = (roster_uri, local)
  end

  module Item = struct
    type t = {
      jid : Xmppl.Jid.t;
      approved : bool option;
      ask : string option;
      name : string option;
      subscription : string option;
      groups : string list;
    }

    let make ?name ?(groups = []) ?subscription ?approved ?ask jid =
      { jid; approved; ask; name; groups; subscription }

    let equal a b =
      Xmppl.Jid.compare a.jid b.jid = 0
      && Option.equal Bool.equal a.approved b.approved
      && Option.equal String.equal a.ask b.ask
      && Option.equal String.equal a.name b.name
      && Option.equal String.equal a.subscription b.subscription
      && List.equal String.equal a.groups b.groups

    let pp =
      Fmt.record
        [
          Fmt.field "jid" (fun r -> r.jid) Xmppl.Jid.pp;
          Fmt.field "name"
            (fun r -> r.name)
            Fmt.(option ~none:(styled `Faint @@ any "None") string);
          Fmt.field "approved"
            (fun r -> r.approved)
            Fmt.(option ~none:(styled `Faint @@ any "None") bool);
          Fmt.field "ask"
            (fun r -> r.ask)
            Fmt.(option ~none:(styled `Faint @@ any "None") string);
          Fmt.field "subscription"
            (fun r -> r.subscription)
            Fmt.(option ~none:(styled `Faint @@ any "None") string);
          Fmt.field "groups" (fun r -> r.groups) Fmt.(list ~sep:comma string);
        ]

    let parser =
      let group_parser =
        Xmlc.Parser.(element (Ns.roster "group") (fun _attributes -> data))
      in
      Xmlc.Parser.(
        element (Ns.roster "item") (fun attributes ->
            let jid =
              Attributes.get_exn ("", "jid") attributes
              |> Xmppl.Jid.of_string_exn
            in
            let approved =
              match Attributes.get ("", "approved") attributes with
              | Some "true" -> Some true
              | Some "false" -> Some false
              | _ -> None
            in
            let ask =
              match Attributes.get ("", "ask") attributes with
              | Some "subscribe" -> Some "subscribe"
              | _ -> None
            in
            let name =
              (* From RFC 6121 (2.4.1):
               * Implementation Note: Including an empty 'name' attribute is
               * equivalent to including no 'name' attribute; both actions set the
               * name to the empty string. *)
              match Attributes.get ("", "name") attributes with
              | Some "" -> None
              | Some name -> Some name
              | _ -> None
            in
            let subscription = Attributes.get ("", "subscription") attributes in
            many group_parser >>| fun groups ->
            make ~groups ?name ?subscription ?approved ?ask jid))
  end

  type client = Client.t
  type roster = Item.t Xmppl.Jid.Map.t

  let get client =
    let* result =
      Client.iq_get client
      @@ Xmlc.Tree.(
           make_element
             ~attributes:
               [ Xmlc.Namespace.default_xmlns_declaration Ns.roster_uri ]
             (Ns.roster "query"))
    in

    match result.payload with
    | Some payload ->
        Xmlc.Tree.parse
          Xmlc.Parser.(
            element (Ns.roster "query") (fun _attributes -> many Item.parser)
            >>| fun roster_l ->
            roster_l |> List.to_seq
            |> Seq.map (fun (item : Item.t) -> (item.jid, item))
            |> Xmppl.Jid.Map.of_seq)
          payload
    | None -> return Xmppl.Jid.Map.empty

  let add_update client ?name ?(groups = []) jid =
    Client.iq_set client
    @@ Xmlc.Tree.(
         make_element
           ~attributes:
             [ Xmlc.Namespace.default_xmlns_declaration Ns.roster_uri ]
           ~children:
             [
               make_element
                 ~attributes:
                   (List.filter_map
                      (fun x -> x)
                      [
                        Some (("", "jid"), Xmppl.Jid.to_string jid);
                        Option.map (fun name -> (("", "name"), name)) name;
                      ])
                 ~children:
                   (List.map
                      (fun group ->
                        make_element
                          ~children:[ make_data group ]
                          (Ns.roster "group"))
                      groups)
                 (Ns.roster "item");
             ]
           (Ns.roster "query"))

  let remove client jid =
    Client.iq_set client
    @@ Xmlc.Tree.(
         make_element
           ~attributes:
             [ Xmlc.Namespace.default_xmlns_declaration Ns.roster_uri ]
           ~children:
             [
               make_element
                 ~attributes:
                   [
                     (("", "jid"), Xmppl.Jid.to_string jid);
                     (("", "subscription"), "remove");
                   ]
                 (Ns.roster "item");
             ]
           (Ns.roster "query"))

  let roster client =
    let* init_roster = get client in
    let* jid = Client.jid client in
    let pushes = Client.iq_sets ~element:(Ns.roster "query") client in
    let parser =
      (* The <query/> element in a roster push MUST contain one and only
       * one <item/> element. *)
      Xmlc.Parser.(element (Ns.roster "query") (fun _attributes -> Item.parser))
    in
    let valid_from from =
      (* A receiving client MUST ignore the stanza unless it has no 'from'
       * attribute (i.e., implicitly from the bare JID of the user's
       * account) or it has a 'from' attribute whose value matches the
       * user's bare JID <user@domainpart>. *)
      match from with
      | None -> true
      | Some jid -> Xmppl.Jid.(compare jid (bare @@ jid) == 0)
    in

    let is_remove (item : Item.t) =
      Option.map (String.equal "remove") item.subscription
      |> Option.value ~default:false
    in

    return
    @@ S.fold_s
         ~eq:(Xmppl.Jid.Map.equal Item.equal)
         (fun roster (push : Xmppl.Stanza.Iq.set) ->
           let* item =
             Xmlc.Tree.parse parser push.payload |> Lwt_result.catch
           in
           match item with
           | Ok item ->
               if valid_from push.from then
                 (* Send a result *)
                 let result =
                   Xmppl.Stanza.Iq.make_result ~id:push.id ?to':push.from
                     ~from:jid None
                 in
                 Client.send_iq client result >|= fun () ->
                 Xmppl.Jid.Map.update item.jid
                   (fun _ -> if is_remove item then None else Some item)
                   roster
               else (* TODO send an error response *)
                 return roster
           | Error _exn ->
               (* TODO send an error response *)
               return roster)
         init_roster pushes

  let presence_subscribe client jid =
    let id = Client.generate_id client in
    Xmppl.Stanza.Presence.make ~id ~type':"subscribe" ~to':jid []
    |> Client.send_presence client

  let presence_unsubscribe client jid =
    let id = Client.generate_id client in
    Xmppl.Stanza.Presence.make ~id ~type':"unsubscribe" ~to':jid []
    |> Client.send_presence client

  let approve_presence_subscription client jid =
    let id = Client.generate_id client in
    Xmppl.Stanza.Presence.make ~id ~type':"subscribed" ~to':jid []
    |> Client.send_presence client

  let deny_presence_subscription client jid =
    let id = Client.generate_id client in
    Xmppl.Stanza.Presence.make ~id ~type':"unsubscribed" ~to':jid []
    |> Client.send_presence client
end
