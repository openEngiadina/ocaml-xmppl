(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react

module Make (Client : Xmppl.Client.S) = struct
  module Ns = struct
    let disco_info_uri = "http://jabber.org/protocol/disco#info"
    let disco_info local = (disco_info_uri, local)
    let disco_items_uri = "http://jabber.org/protocol/disco#items"
    let disco_items local = (disco_items_uri, local)
  end

  module Identity = struct
    type t = {
      category : string;
      type' : string;
      name : string option;
      lang : string option;
    }

    let create ?name ?lang category type' = { category; type'; name; lang }

    let parser =
      Xmlc.Parser.(
        element (Ns.disco_info "identity") (fun attrs ->
            return
              {
                category = Attributes.get_exn ("", "category") attrs;
                type' = Attributes.get_exn ("", "type") attrs;
                name = Attributes.get ("", "name") attrs;
                lang = Attributes.get (Xmlc.Namespace.xml "lang") attrs;
              }))

    let pp =
      Fmt.record
        [
          Fmt.field "category" (fun r -> r.category) Fmt.string;
          Fmt.field "type" (fun r -> r.type') Fmt.string;
          Fmt.field "name"
            (fun r -> r.name)
            Fmt.(option ~none:(styled `Faint @@ any "None") string);
          Fmt.field "xml:lang"
            (fun r -> r.lang)
            Fmt.(option ~none:(styled `Faint @@ any "None") string);
        ]

    let to_xml identity =
      Xmlc.Tree.make_element
        ~attributes:
          List.(
            filter_map
              (fun x -> x)
              [
                Some (Ns.disco_info "category", identity.category);
                Some (Ns.disco_info "type", identity.type');
                Option.map
                  (fun name -> (Ns.disco_info "name", name))
                  identity.name;
                Option.map
                  (fun lang -> (Xmlc.Namespace.xml "lang", lang))
                  identity.lang;
              ])
        (Ns.disco_info "identity")
  end

  module Feature = struct
    type t = string

    let to_xml f =
      Xmlc.Tree.make_element
        ~attributes:[ (Ns.disco_info "var", f) ]
        (Ns.disco_info "feature")

    let parser =
      Xmlc.Parser.(
        element (Ns.disco_info "feature") (Attributes.required ("", "var")))
  end

  type info_result = Identity of Identity.t | Feature of Feature.t

  let pp_info_result ppf = function
    | Identity identity -> Fmt.pf ppf "@[Identity: %a@]" Identity.pp identity
    | Feature feature -> Fmt.pf ppf "@[Feature: %s@]" feature

  let info_result_parser =
    Xmlc.Parser.(
      element (Ns.disco_info "query") (fun _attrs ->
          many
          @@ choice ~failure_msg:"Invalid element in DISCO info result"
               [
                 (Identity.parser >>| fun i -> Identity i);
                 (Feature.parser >>| fun f -> Feature f);
               ]))

  let query_info ~to' ?node client =
    let req =
      Xmlc.Tree.make_element
        ~attributes:
          List.(
            filter_map
              (fun x -> x)
              [
                Some
                  (Xmlc.Namespace.default_xmlns_declaration Ns.disco_info_uri);
                Option.map (fun node -> (Ns.disco_info "node", node)) node;
              ])
        (Ns.disco_info "query")
    in

    let* result = Client.iq_get client ~to' req in

    match result.payload with
    | Some payload -> Xmlc.Tree.parse info_result_parser payload
    | None -> return_nil

  module Item = struct
    type t = {
      jid : string;
      name : string option;
      attributes : Xmlm.attribute list;
    }

    let parser =
      Xmlc.Parser.(
        element (Ns.disco_items "item") (fun attrs ->
            return
              {
                jid = Attributes.get_exn ("", "jid") attrs;
                name = Attributes.get ("", "name") attrs;
                attributes = attrs;
              }))

    let pp =
      Fmt.record
        [
          Fmt.field "jid" (fun r -> r.jid) Fmt.string;
          Fmt.field "name"
            (fun r -> r.name)
            Fmt.(option ~none:(styled `Faint @@ any "None") string);
          Fmt.field "attributes"
            (fun r -> r.attributes)
            (Fmt.list Xmlm.pp_attribute);
        ]
  end

  let items_result_parser =
    Xmlc.Parser.(
      element (Ns.disco_items "query") (fun _attrs -> many @@ Item.parser))

  let query_items ~to' ?node client =
    let req =
      Xmlc.Tree.make_element
        ~attributes:
          List.(
            filter_map
              (fun x -> x)
              [
                Some
                  (Xmlc.Namespace.default_xmlns_declaration Ns.disco_items_uri);
                Option.map (fun node -> (Ns.disco_items "node", node)) node;
              ])
        (Ns.disco_items "query")
    in

    let* result = Client.iq_get client ~to' req in

    match result.payload with
    | Some payload -> Xmlc.Tree.parse items_result_parser payload
    | None -> return_nil

  let info_requests client node =
    Client.iq_gets ~element:(Ns.disco_info "query") client
    |> E.map (fun (get : Xmppl.Stanza.Iq.get) ->
           match Xmlc.Tree.get_root_attribute ("", "node") get.payload with
           | Some req_node ->
               if String.equal req_node node then Some get else None
           | None -> None)
    |> E.filter Option.is_some |> E.map Option.get
end
