(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type S = sig
  type client_initial_message

  val client_initial_message : username:string -> client_initial_message
  val encode_client_initial_message : client_initial_message -> string

  type server_challenge

  val decode_server_challenge : string -> (server_challenge, string) Result.t
  val pp_server_challenge : server_challenge Fmt.t

  type client_response

  val client_response :
    password:string ->
    client_initial_message ->
    server_challenge ->
    (client_response, string) Result.t

  val encode_client_response : client_response -> string

  val check_server_response :
    client_response -> string -> (unit, string) Result.t
end

module Make (H : Digestif.S) : S
