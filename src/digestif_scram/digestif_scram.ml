(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type S = sig
  type client_initial_message

  val client_initial_message : username:string -> client_initial_message
  val encode_client_initial_message : client_initial_message -> string

  type server_challenge

  val decode_server_challenge : string -> (server_challenge, string) Result.t
  val pp_server_challenge : server_challenge Fmt.t

  type client_response

  val client_response :
    password:string ->
    client_initial_message ->
    server_challenge ->
    (client_response, string) Result.t

  val encode_client_response : client_response -> string

  val check_server_response :
    client_response -> string -> (unit, string) Result.t
end

module Make (H : Digestif.S) : S = struct
  module Pbkdf = Digestif_pbkdf.Make (H)

  (* FIXME This should implement the stringprep algorithm [RFC3454] with the
   * SASLprep profile [RFC4013]. However currently it only does Unicode KC
   * normalization.
   *
   * The [Camomile](https://github.com/yoriyuki/Camomile) library has a proper stringrep implementaiton.
   *
   * [RFC3454] https://datatracker.ietf.org/doc/html/rfc3454
   * [RFC4013] https://datatracker.ietf.org/doc/html/rfc4013
   *
   * *)
  let stringprep s = Uunf_string.normalize_utf_8 `NFKC s

  let random_nonce () =
    String.init 12 (fun _i -> Char.chr @@ Random.int 255)
    |> Base64.encode_string

  type client_initial_message = { username : string; client_nonce : string }

  let client_initial_message ~username =
    { username = stringprep username; client_nonce = random_nonce () }

  let encode_client_initial_message im =
    "n,," ^ "n=" ^ im.username ^ ",r=" ^ im.client_nonce |> Base64.encode_string

  type server_challenge = {
    server_nonce : string;
    salt : string;
    iterations : int;
  }

  let server_challenge_parser =
    let is_comma = Char.equal ',' in
    Angstrom.(
      (fun _ server_nonce salt iterations -> { server_nonce; salt; iterations })
      <$> string "r=" <*> take_till is_comma <* string ",s="
      <*> (take_till is_comma >>| Base64.decode_exn)
      <* string ",i="
      <*> (take_while (fun _ -> true) >>| int_of_string))

  let decode_server_challenge s =
    Angstrom.parse_string ~consume:Angstrom.Consume.All server_challenge_parser
      (Base64.decode_exn s)

  let pp_server_challenge ppf sc =
    Fmt.pf ppf "@[r=%s,s=%s,i=%d@]" sc.server_nonce
      (Base64.encode_string sc.salt)
      sc.iterations

  type client_response = { final_message : string; server_signature : string }

  let xor a b =
    String.mapi
      (fun i ca ->
        let cb = String.get b i in
        Char.chr @@ (Char.code ca lxor Char.code cb))
      a

  let client_response ~password cim sc =
    if
      String.equal cim.client_nonce
        (String.sub sc.server_nonce 0 (String.length cim.client_nonce))
    then
      let client_final_message_bare = "c=biws,r=" ^ sc.server_nonce in
      let normalized_password = stringprep password in
      let salted_password =
        Pbkdf.pbkdf2
          ~password:(Cstruct.of_string normalized_password)
          ~dk_len:(Int32.of_int H.digest_size)
          ~salt:(Cstruct.of_string sc.salt)
          ~count:sc.iterations
        |> Cstruct.to_string
      in
      let client_key =
        H.hmac_string ~key:salted_password "Client Key" |> H.to_raw_string
      in
      let stored_key = H.digest_string client_key |> H.to_raw_string in
      let auth_message =
        "n=" ^ cim.username ^ ",r=" ^ cim.client_nonce ^ ",r=" ^ sc.server_nonce
        ^ ",s="
        ^ Base64.encode_string sc.salt
        ^ ",i="
        ^ string_of_int sc.iterations
        ^ "," ^ client_final_message_bare
      in
      let client_signature =
        H.hmac_string ~key:stored_key auth_message |> H.to_raw_string
      in
      let client_proof = xor client_key client_signature in
      let client_final_message =
        client_final_message_bare ^ ",p=" ^ Base64.encode_string client_proof
      in
      let server_key =
        H.hmac_string ~key:salted_password "Server Key" |> H.to_raw_string
      in
      let server_signature =
        H.hmac_string ~key:server_key auth_message |> H.to_raw_string
      in
      Ok { final_message = client_final_message; server_signature }
    else Error "client nonce is not prefix of server nonce"

  let encode_client_response cr = Base64.encode_string cr.final_message

  let check_server_response cr s =
    let server_response_parser =
      Angstrom.(
        string "v=" *> (take_while (fun _ -> true) >>| Base64.decode_exn))
    in

    let decode_server_repsonse s =
      Angstrom.parse_string ~consume:Angstrom.Consume.All server_response_parser
        (Base64.decode_exn s)
    in

    Result.bind (decode_server_repsonse s) (fun decoded_signature ->
        if String.equal decoded_signature cr.server_signature then Ok ()
        else Error "invalid server signature")
end
