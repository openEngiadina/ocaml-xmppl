(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type options = { ws_endpoint : string option }

val default_options : options

module Client : Xmppl.Client.S with type transport_options = options
