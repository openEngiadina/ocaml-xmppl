(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)
open Lwt
open Lwt.Syntax
open Brr
open Brr_io

let src = Logs.Src.create "Xmpp_websocket"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)

(* Parser XML from a string (to a list of signals) *)
let parse_xml msg =
  let input = Xmlm.make_input ~strip:true (`String (0, msg)) in
  let rec loop signals =
    if Xmlm.eoi input then List.rev signals
    else
      match Xmlm.input input with
      (* ignore the Dtd signal. the XMPP parsers do not expect it *)
      | `Dtd _ -> loop signals
      | s -> loop @@ (s :: signals)
  in
  try loop [] with e -> raise e

(* Implements just enough of RFC 7395 for our use *)
module WebHostMetadata = struct
  type link = { rel : string; href : string }

  (* Helper to transform Brr.Fut to Lwt.t *)
  let fut_as_lwt fut =
    let promise, wake = Lwt.wait () in
    Fut.await fut (fun v -> Lwt.wakeup wake v);
    promise

  let fut_or_error_as_lwt fut =
    fut_as_lwt fut >>= function
    | Ok v -> return v
    | Error e -> fail @@ Jv.Error e

  (* Helper to read response body to a string *)
  let response_body_as_string response =
    Fetch.Response.as_body response
    |> Fetch.Body.text |> fut_or_error_as_lwt >|= Jstr.to_string

  (* the XRD namespace *)
  let xrd local = ("http://docs.oasis-open.org/ns/xri/xrd-1.0", local)

  let parser_link =
    Xmlc.Parser.(
      element (xrd "Link") (fun attributes ->
          let rel = Attributes.get_exn ("", "rel") attributes in
          let href = Attributes.get_exn ("", "href") attributes in
          return { rel; href }))

  let parser = Xmlc.Parser.(element (xrd "XRD") (fun _ -> many parser_link))

  let get domain =
    let url = "https://" ^ domain ^ "/.well-known/host-meta" in
    let* response_s =
      fut_or_error_as_lwt @@ Fetch.url (Jstr.v url) >>= response_body_as_string
    in
    (* There is a clash of the term "parse". We first parse from string to signals and then from signals to OCaml values. *)
    parse_xml response_s |> Lwt_stream.of_list
    |> Xmlc.Parser.parse_stream parser
end

type options = { ws_endpoint : string option }

let default_options = { ws_endpoint = None }

module Transport = struct
  type nonrec options = options

  type t = {
    ws : Websocket.t;
    msg_stream : string Lwt_stream.t;
    closed : unit Lwt.t;
  }

  (* Custom type for the WebSocket open event *)
  type websocket_open

  let websocket_open : websocket_open Ev.type' = Ev.Type.create (Jstr.v "open")

  (* Find a suitable websocket endpoint using XEP-0156 (Discovering Alternative XMPP Connection Methods) *)
  let resolve_xep_0156 host =
    let* host_metadata = WebHostMetadata.get host in
    let ws_link_opt =
      List.find_map
        (fun (link : WebHostMetadata.link) ->
          if link.rel = "urn:xmpp:alt-connections:websocket" then Some link.href
          else None)
        host_metadata
    in
    match ws_link_opt with
    | Some endpoint -> return endpoint
    | None ->
        fail_with
          "Could not resolve suitable WebSocket endpoint (is the host \
           meta-data set up propertly according to XEP-0156?)"

  let resolve options host =
    match options.ws_endpoint with
    | Some endpoint -> return @@ Jstr.v endpoint
    | None -> resolve_xep_0156 host >|= Jstr.v

  let connect ~host options =
    let* endpoint = resolve options host in

    let* () =
      Log.debug (fun m ->
          m "Opening WebSocket connection to %s" (Jstr.to_string endpoint))
    in

    let ws = Websocket.create ~protocols:[ Jstr.of_string "xmpp" ] endpoint in

    let opened, on_open = Lwt.wait () in
    Fut.await
      (Ev.next websocket_open (Websocket.as_target ws))
      (fun _ -> Lwt.wakeup on_open ());

    (* Setup reading stream *)
    let msg_stream, push_msg = Lwt_stream.create () in
    Ev.listen Message.Ev.message
      (fun message_t ->
        let msg = message_t |> Ev.as_type |> Message.Ev.data |> Jv.to_string in
        push_msg (Some msg))
      (Websocket.as_target ws);

    (* (\* Watch for close event *\) *)
    let closed, on_close = Lwt.wait () in
    Ev.listen Websocket.Ev.close
      (fun _close_t ->
        Lwt.wakeup on_close ();
        push_msg None)
      (Websocket.as_target ws);

    opened >|= fun _ -> { ws; msg_stream; closed }

  let close t = return @@ Websocket.close t.ws
  let closed t = t.closed

  type stream = {
    id : string;
    signals : Xmlm.signal Lwt_stream.t;
    send : Xmlc.Tree.t -> unit Lwt.t;
    stop : unit -> unit Lwt.t;
  }

  let open_stream t ~to' =
    let is_stopped, stop_wakeup = Lwt.wait () in

    let stop () =
      Lwt.wakeup stop_wakeup None;
      return_unit
    in

    (* Setup stream of incoming XML signals *)
    let signals, push_signal, set_ref = Lwt_stream.create_with_reference () in
    let rec read_p () =
      let* v = choose [ is_stopped; Lwt_stream.get t.msg_stream ] in
      match v with
      | None ->
          let* () = Log.debug (fun m -> m "Stopping XML stream.") in
          return @@ push_signal None
      | Some msg ->
          parse_xml msg |> List.iter (fun s -> push_signal (Some s));
          read_p ()
    in
    set_ref (read_p ());

    (* Attach a debugger to incoming signals *)
    (* Lwt_react.E.map_s
     *   (fun signal ->
     *     Log.debug (fun m -> m "RECV: %a" Xmlc.pp_signal signal))
     *   (signals |> Lwt_stream.clone |> Lwt_react.E.of_stream)
     * |> Lwt_react.E.keep; *)
    let xml_to_string xml =
      let buffer = Buffer.create 60 in

      let output = Xmlm.make_output ~decl:false (`Buffer buffer) in

      (* Xmlm requirs a Dtd  *)
      Xmlm.output output (`Dtd None);

      xml |> Xmlc.Tree.to_seq
      |> Seq.iter (fun signal -> Xmlm.output output signal);

      String.of_seq @@ Buffer.to_seq buffer
    in

    let send xml =
      let* msg = try return @@ xml_to_string xml with e -> fail e in

      (* let* () = Log.debug (fun m -> m "SEND: %s" msg) in *)
      msg |> Jstr.of_string |> Websocket.send_string t.ws |> return
    in

    let* () =
      send
        Xmlc.Tree.(
          make_element
            ~attributes:
              [
                Xmlc.Namespace.default_xmlns_declaration
                  Xmppl.Namespace.framing_uri;
                (Xmppl.Namespace.framing "to", to');
                (Xmppl.Namespace.framing "version", "1.0");
              ]
            ~children:[]
            (Xmppl.Namespace.framing "open"))
    in

    let stream_id_parser =
      Xmlc.Parser.(
        element
          (Xmppl.Namespace.framing "open")
          (Attributes.required ("", "id")))
    in

    (* Parse the stream id from stream header *)
    let* id = Xmlc.Parser.parse_stream stream_id_parser signals in

    let* () = Log.debug (fun m -> m "XMPP Stream initiated (id: %s)" id) in

    return { id; signals; stop; send }

  let stream_id stream = return stream.id
  let send_xml stream xml = stream.send xml
  let signals stream = stream.signals
  let stop_stream stream = stream.stop ()
end

module Client = Xmppl.Client.Make (Transport)
