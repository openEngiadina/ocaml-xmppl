(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type options = {
  port : int;
  disable_ssl : bool;
  incoming_signals : (Xmlm.signal -> unit) option;
}

val default_options : options

module Client : Xmppl.Client.S with type transport_options = options
