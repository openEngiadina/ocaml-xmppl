(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

let src = Logs.Src.create "Xmppl_unix"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)

type options = {
  port : int;
  disable_ssl : bool;
  incoming_signals : (Xmlm.signal -> unit) option;
}

let default_options =
  { port = 5223; disable_ssl = false; incoming_signals = None }

module Transport = struct
  type nonrec options = options

  type t = {
    options : options;
    socket : Lwt_ssl.socket;
    stream : char Lwt_stream.t;
  }

  let connect ~host options =
    (* Initialize SSL *)
    Ssl.init ();

    (* Create a Unix socket *)
    let fd = Lwt_unix.(socket PF_INET SOCK_STREAM 0) in

    (* Resolve host name to IP address *)
    let* host_entry = Lwt_unix.gethostbyname host in

    let* inet_addr =
      match host_entry.h_addr_list with
      | [| inet_addr |] -> return inet_addr
      | _ -> fail_with "Could not resolve host name"
    in

    let* () =
      Log.debug (fun m ->
          m "Opening TCP socket to %s (port: %d; disable_ssl: %b)"
            (Unix.string_of_inet_addr inet_addr)
            options.port options.disable_ssl)
    in

    (* Connect the socket *)
    let* () = Lwt_unix.(connect fd (ADDR_INET (inet_addr, options.port))) in

    let ssl_context = Ssl.(create_context TLSv1_3 Client_context) in

    let* socket =
      if options.disable_ssl then return @@ Lwt_ssl.plain fd
      else Lwt_ssl.ssl_connect fd ssl_context
    in

    (* convert socket to input channel *)
    (* let ic = Lwt_io.(of_fd ~mode:Input sock) in *)
    let ic = Lwt_ssl.in_channel_of_descr socket in

    (* and then to stream *)
    let stream = Lwt_stream.from (fun () -> Lwt_io.read_char_opt ic) in

    return { socket; stream; options }

  let close t = Lwt_ssl.close t.socket
  let closed t = Lwt_stream.closed t.stream

  type stream = {
    id : string;
    signals : Xmlm.signal Lwt_stream.t;
    send : Xmlc.Tree.t -> unit Lwt.t;
    stop : unit -> unit Lwt.t;
  }

  let open_stream t ~to' =
    let is_stopped, stop_wakeup = Lwt.wait () in

    let stop () =
      Lwt.wakeup stop_wakeup None;
      return_unit
    in

    (* Setup stream of incoming XML signals *)
    let in_stream, in_push, set_ref = Lwt_stream.create_with_reference () in
    let rec read_p () =
      let* v = choose [ is_stopped; Lwt_stream.get t.stream ] in
      match v with
      | None -> return @@ in_push None
      | Some _ ->
          in_push v;
          read_p ()
    in
    set_ref (read_p ());

    let signals =
      in_stream |> Markup_lwt.lwt_stream |> Markup_lwt.parse_xml
      |> Markup.signals |> Markup_lwt.to_lwt_stream
      |> Lwt_stream.filter_map (function
           | `Start_element t -> Some (`El_start t)
           | `Text s -> Some (`Data (String.concat "" s))
           | `End_element -> Some `El_end
           | `Xml _dec -> None
           | _ -> None)
    in

    let to_markup_signal (s : Xmlm.signal) : Markup.signal =
      match s with
      | `Dtd _ -> `Xml { version = "1.0"; encoding = None; standalone = None }
      | `El_start t -> `Start_element t
      | `Data s -> `Text [ s ]
      | `El_end -> `End_element
    in

    (* Attach a debugger to incoming signals *)
    let () =
      match t.options.incoming_signals with
      | Some incoming_signals ->
          Lwt_react.E.map incoming_signals
            (signals |> Lwt_stream.clone |> Lwt_react.E.of_stream)
          |> Lwt_react.E.keep
      | None -> ()
    in

    (* Setup outgoing stream *)
    let out_stream, out_push, set_ref = Lwt_stream.create_with_reference () in
    let send_p =
      out_stream
      |> Lwt_stream.map to_markup_signal
      (* |> Lwt_stream.map_s (fun signal ->
       *        let* () =
       *          Log.debug (fun m ->
       *              m "SEND: %s" (Markup.signal_to_string signal))
       *        in
       *        return signal) *)
      |> Markup_lwt.lwt_stream
      |> Markup_lwt.write_xml
      |> Markup_lwt.fold
           (fun () c ->
             let b = Bytes.of_seq @@ Seq.return c in
             Lwt_ssl.write t.socket b 0 1 >|= fun _ -> ())
           ()
    in
    set_ref send_p;

    let send xml =
      xml |> Xmlc.Tree.to_seq |> List.of_seq
      |> Lwt_list.fold_left_s
           (fun () signal -> return @@ out_push (Some signal))
           ()
    in

    let send_signal signal = out_push (Some signal) in

    (* Send XML declaration *)
    let xml_dec : Xmlm.signal = `Dtd None in
    send_signal xml_dec;

    (* Send Stream header *)
    let header =
      `El_start
        ( Xmppl.Namespace.streams "stream",
          [
            (Xmppl.Namespace.client "to", to');
            (Xmppl.Namespace.client "version", "1.0");
            Xmlc.Namespace.default_xmlns_declaration Xmppl.Namespace.client_uri;
            (Xmlc.Namespace.xmlns "stream", Xmppl.Namespace.streams_uri);
          ] )
    in
    send_signal header;

    (* Send whitespace to force flushing of stream header *)
    send_signal (`Data "");

    let stream_id_parser =
      Xmlc.Parser.(
        el_start (Xmppl.Namespace.streams "stream")
        >>= Attributes.required ("", "id"))
    in

    (* Parse an XML declaration *)
    (* let* _ = Xmlc.Parser.parse_stream Xmlc.Parser.dtd signals in *)

    (* Parse the stream id from stream header *)
    let* id = Xmlc.Parser.parse_stream stream_id_parser signals in
    let* () = Log.debug (fun m -> m "XMPP Stream initiated (id: %s)" id) in

    return { id; signals; send; stop }

  let stream_id stream = return stream.id
  let send_xml stream xml = stream.send xml
  let signals stream = stream.signals
  let stop_stream stream = stream.stop ()
end

module Client = Xmppl.Client.Make (Transport)
