(*
 * SPDX-FileCopyrightText: 2021, 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** Parser combinators and tree representations for XML. *)

(** This module provides utility functions for dealing with XML:

- A parser combinator library for parsing structured OCaml values from
  streams of XML signals.
- Types and functions for working with XML as trees.
- Common namespaces

Types for Xml signals are shared with the {!Xmlm} library. This module can also be used with other XML parser (e.g. Markup.ml) by converting signals.

 *)

module Parser : sig
  (** Parser Combinators for XML signals *)

  type 'a t
  (** A parser that returns value of type ['a]. *)

  (** {1 Exceptions} *)

  exception Failure of string * Xmlm.signal option
  (** Raised when parsing fails. *)

  exception End_of_stream
  (** Raised when end of stream is encountered unexpectedly. *)

  (** {1 Combinators} *)

  val return : 'a -> 'a t
  (** [return v] creates a parser that will always succeed and return
  [v]. *)

  val fail : exn -> 'a t
  (** [fail exn] creates a parser that will always fail with the exception [exn]. *)

  val fail_with : ?signal:Xmlm.signal -> string -> 'a t
  (** [fail_with msg] creates a parser that will always fail with the
  message [msg]. If an optional [signal] is provided the error message
  is annotated with [signal]. *)

  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
  (** [p >>= f] creates a parser that will run [p], pass its result to
  [f] and run the parser that [f] produces, and returns its result. *)

  val ( >>| ) : 'a t -> ('a -> 'b) -> 'b t
  (** [p >>| f] creates a parser that will run [p], and if it succeeds
  with result [v], will return [f v] *)

  val ( <$> ) : ('a -> 'b) -> 'a t -> 'b t
  (** [f <$> p] is equivalent to [p >>| f] *)

  val ( <*> ) : ('a -> 'b) t -> 'a t -> 'b t
  (** [f <*> p] is equivalent to [f >>= fun f -> p >>| f]. *)

  val ( *> ) : _ t -> 'a t -> 'a t
  (** [p <* q] runs [p], then runs [q], discards its result, and
  returns the result of [p]. *)

  val ( <* ) : 'a t -> _ t -> 'a t
  (** [p <* q] runs [p], then runs [q], discards its result, and
  returns the result of [p]. *)

  val ( <?> ) : 'a t -> string -> 'a t
  (** [p <?> msg] returns a parser that runs [p] and returns the
  result of [p] if [p] succeeds. If [p] fails the parser fails with
  message [msg]. *)

  val ( <|> ) : 'a t -> 'a t -> 'a t
  (** [p <|> q] returns a parser that runs [p] and returns the result
  of [p] if [p] succeeds. If [p] fails [q] is run. *)

  val choice : ?failure_msg:string -> 'a t list -> 'a t
  (** [choice ?failure_msg ts] runs each parser in [ts] in order until
  one succeeds and returns that result. In the case that none of the
  parser succeeds, then the parser will fail with the message
  [failure_msg], if provided, or a much less informative message
  otherwise. *)

  val option : 'a t -> 'a option t
  (** [option p] returns a parser that runs [p] and returns the result
  of [p] as [Some _] or [None] if [p] fails. *)

  val fix : ('a t -> 'a t) -> 'a t
  (** [fix f] computes the fixpoint of [f] and runs the resultant
  parser. The argument that [f] receives is the result of [fix f],
  which [f] must use, paradoxically, to define [fix f]. *)

  val many : 'a t -> 'a list t
  (** [many p] runs [p] {i zero} or more times and returns a list of
  results from the runs of [p]. *)

  val stream_end : unit t
  (** [stream_end] succeeds if the end of input is reached and fails otherwise. *)

  (** {1 XML Signal Parsers} *)

  val dtd : string option t
  (** [dtd] returns a parser that returns a XML DTD definition. *)

  val data : string t
  (** [data] reads an XML data signal. *)

  val any_el_start : Xmlm.tag t
  (** [any_el_start] reads an XML element start and returns the tag. *)

  val el_start : Xmlm.name -> Xmlm.attribute list t
  (** [el_start name] returns a parser that reads an XML element start
  with [name] and returns the attributes. *)

  val el_end : unit t
  (** [el_end] reads an XML element end signal. *)

  (** {1 XML Element parsers} *)

  (** These are useful combinations of the XML signal parsers. *)

  val element : Xmlm.name -> (Xmlm.attribute list -> 'b t) -> 'b t
  (** [element name f] returns a parser that reads an XML element with
  [name] and runs the parer resulting by apply [f] on the attributes
  of the XML element followed by an [el_end] parser. *)

  val any_element : (Xmlm.tag -> 'a t) -> 'a t
  (** [any_element name f] returns a parser that reads any XML element
  and runs the parer resulting by apply [f] on the tag of the XML
  element followed by an [el_end] parser. *)

  val ignore_any : unit t
  (** [ignore_any] reads one XML element or data item and ignores it. *)

  val ignore_element : unit t
  (** [ignore_element] reads one XML and ignores it. *)

  val complex :
    ?required:Xmlm.name list ->
    ?ignore_other:bool ->
    Xmlm.name ->
    (Xmlm.attribute list -> 'a t) ->
    (Xmlm.name * ('a -> 'a)) t list ->
    'a t
  (** [complex ~required name root children] returns a parser for a
  complex object [name] that is represented with multiple XML child
  elements that appear in an unspecified order. The [root] parser is
  applied to the root element and provides a base object.

  If the root element does not have any children [complex] will return
  the same parser as [root] (if no required fields are specified).

  Child elements are parsed with the first succeeding parser in
  [children] whose elements returns a transformation of the base
  object.

  If a [required] argument is provided the returned parser will fail
  if there is no child with given name for every member of [required].

  If [ignore_others] (defaults: false) is set then child elements that
  can not be parsed with parsers in [children] are ignored.

  [complex] is useful for parsing complex data types such as Atom
  entries, where there are many optional child elements that may
  appear in unspecified order.
   *)

  module Attributes : sig
    (** Helpers for getting attribute values *)

    val get : Xmlm.name -> Xmlm.attribute list -> string option
    (** [get name attributes] returns the attribute value for [name]
    in [attributes]. *)

    val get_exn : Xmlm.name -> Xmlm.attribute list -> string
    (** [get_exn name attributes] returns the attribute value for
    [name] in [attributes].

    @raise Failure if the attribute is not present.*)

    val optional : Xmlm.name -> Xmlm.attribute list -> string option t
    (** [optional name attributes] returns a parser that returns the
    attribute value for [name] in [attributes]. *)

    val required : Xmlm.name -> Xmlm.attribute list -> string t
    (** [required name attributes] returns a parser that returns the
    attribute value for [name] in [attributes] and fails if the
    attribute is not present. *)
  end

  val parse_stream : 'a t -> Xmlm.signal Lwt_stream.t -> 'a Lwt.t
end

module Tree : sig
  (** Tree representation of XML *)

  type t =
    | Element of {
        name : Xmlm.name;
        attributes : Xmlm.attribute list;
        children : t list;
      }
    | Data of string  (** Type of a XML tree *)

  val make_element :
    ?attributes:Xmlm.attribute list -> ?children:t list -> Xmlm.name -> t
  (** [make_element ?attributes ?children name] returns a new element
node of an XML tree with [name] and optional [children] and
[attributes]. *)

  val make_data : string -> t
  (** [make_data v] returns a new data element of an XML tree with value
[v]. *)

  val to_seq : t -> Xmlm.signal Seq.t
  (** [to_seq tree] returns a sequence of XML signals that represent [tree]. *)

  val pp : t Fmt.t
  (** [pp] returns a pretty printer. 

Currently this uses Xmlm to return an XML reprepresentation. In the
future this might be made prettier with coloured printing and such. *)

  (** {1 Parser} *)

  val parser : t Parser.t
  (** [parser] is a Xmlc parser that can be used to parse a XML tree from a stream of XML signals. *)

  (** {1 Parsing from XML trees} *)

  val parse : 'a Parser.t -> t -> 'a Lwt.t
  (** [parse parser tree] applies [parser] on [tree]. *)

  val parse_trees : 'a Parser.t -> t Seq.t -> 'a Lwt.t
  (** [parse_trees parser forrest] applies [parser[ on a sequence of
trees [forrest]. *)

  (** {1 Miscellaneous } *)
  val get_root_name : t -> Xmlm.name option
  (** [get_root_name tree] returns the name of the root element of
  [tree]. *)

  val get_root_attribute : Xmlm.name -> t -> string option
  (** [get_root_attribute name tree] returns the attribute value of
attribute [name] from the root element of [tree]. *)
end

(** {1 Namespaces} *)

module Namespace : sig
  val xml : string -> Xmlm.name
  val xmlns : string -> Xmlm.name
  val default_xmlns_declaration : string -> Xmlm.attribute
end
