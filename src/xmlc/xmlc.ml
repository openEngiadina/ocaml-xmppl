(*
 * SPDX-FileCopyrightText: 2021, 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module Parser = struct
  type stream = Xmlm.signal Lwt_stream.t
  type 'a t = stream -> 'a Lwt.t

  module IO = struct
    (* Abstract all the Lwt stuff here. *)
    let return = Lwt.return
    let fail = Lwt.fail
    let map = Lwt.map
    let bind = Lwt.bind
    let bind_result o f = Lwt.bind (Lwt_result.catch o) f

    (* reading from stream *)
    let peek = Lwt_stream.peek
    let junk = Lwt_stream.junk
  end

  let return v _stream = IO.return v
  let fail e _stream = IO.fail e

  exception Failure of string * Xmlm.signal option
  exception End_of_stream

  let () =
    Printexc.register_printer (function
      | Failure (msg, None) ->
          Some (Printf.sprintf "Failure [Xmlc.Parser] (%s)" msg)
      | Failure (msg, Some signal) ->
          Some
            (Format.asprintf "Failure [Xmlc.Parser] (%s at %a)" msg
               Xmlm.pp_signal signal)
      | _ -> None)

  let fail_with ?signal msg = fail @@ Failure (msg, signal)

  let bind (p : 'a t) (f : 'a -> 'b t) stream =
    IO.bind (p stream) (fun v -> (f v) stream)

  let map p f stream = IO.map f (p stream)

  (* Syntax (same as Angstrom) *)

  let ( >>= ) p f = bind p f
  let ( >>| ) p f = map p f
  let ( <*> ) f p = f >>= fun f -> p >>| f
  let ( <$> ) f m = m >>| f
  let ( *> ) a b = a >>= fun _ -> b

  let ( <* ) a b =
    a >>= fun x ->
    b >>| fun _ -> x

  let ( <?> ) p msg stream =
    IO.bind_result (p stream) (function
      | Ok v -> IO.return v
      | Error _ -> IO.fail @@ Failure (msg, None))

  let ( <|> ) p q stream =
    IO.bind_result (p stream) (function
      | Ok v -> IO.return v
      | Error _ -> q stream)

  let choice ?(failure_msg = "no more choices") ps =
    List.fold_right ( <|> ) ps (fail_with failure_msg)

  let option p = p >>| Option.some <|> return None

  (* TODO this will probably not work with js_of_ocaml. Check fix_lazy in Angstrom. *)
  let fix f =
    let rec p = lazy (f r) and r signals = (Lazy.force p) signals in
    r

  let many p =
    let cons x xs = x :: xs in
    fix (fun m -> cons <$> p <*> m <|> return [])

  (* XML signals *)

  let stream_end =
    bind IO.peek (function
      | Some s -> fail_with ~signal:s "expecting end of stream"
      | None -> return ())

  let dtd =
    bind IO.peek (function
      | Some (`Dtd v) -> bind IO.junk (fun () -> return v)
      | Some s -> fail_with ~signal:s "expecting dtd"
      | None -> fail End_of_stream)

  let data =
    bind IO.peek (function
      | Some (`Data v) -> bind IO.junk (fun () -> return v)
      | Some s -> fail_with ~signal:s "expecting text"
      | None -> fail End_of_stream)

  let any_el_start =
    bind IO.peek (function
      | Some (`El_start tag) -> bind IO.junk (fun () -> return tag)
      | Some s -> fail_with ~signal:s "expecting a start element"
      | None -> fail End_of_stream)

  let el_start name =
    bind IO.peek (function
      | Some (`El_start (n, attributes)) when n = name ->
          bind IO.junk (fun () -> return attributes)
      | Some s ->
          fail_with ~signal:s
            (Format.asprintf "expecting a start element (%a)" Xmlm.pp_name name)
      | None -> fail End_of_stream)

  let el_end =
    bind IO.peek (function
      | Some `El_end -> bind IO.junk (fun () -> return ())
      | Some s -> fail_with ~signal:s "expecting an end element"
      | None -> fail End_of_stream)

  let element name f =
    el_start name >>= fun attributes -> f attributes <* el_end

  let any_element f = any_el_start >>= f <* el_end

  let ignore_any =
    fix (fun m ->
        data
        >>| (fun _ -> ())
        <|> any_element (fun _tag -> many m >>| fun _ -> ()))

  let ignore_element = any_element (fun _tag -> many ignore_any >>| fun _ -> ())

  let complex ?(required = []) ?(ignore_other = false) (name : Xmlm.name)
      (root : Xmlm.attribute list -> 'a t)
      (children : (Xmlm.name * ('a -> 'a)) t list) =
    element name (fun attributes ->
        (* Use root parser to get a base object *)
        root attributes >>= fun base_object ->
        (* Attempt to parse the child elements *)
        let children =
          if ignore_other then
            (* Add a parser that parses any unknown element to the identity *)
            children @ [ (ignore_element >>| fun () -> (("", ""), fun x -> x)) ]
          else children
        in
        many @@ choice children
        (* Check that all required children have been parsed *)
        >>= fun fields ->
        let missing =
          List.filter
            (fun required_name -> not @@ List.mem_assoc required_name fields)
            required
        in
        (if List.length missing = 0 then return fields
        else
          fail_with
          @@ Format.asprintf
               "Required child elements are missing (%a) while parsing element \
                %a"
               (Fmt.list Xmlm.pp_name) missing Xmlm.pp_name name)
        (* Build the object by applying all the parsed transformations. *)
        >>|
        fun fields ->
        List.fold_left (fun object' (_name, f) -> f object') base_object fields)

  module Attributes = struct
    let get name attributes =
      attributes
      |> List.find_map (function
           | n, value when n = name -> Some value
           | _ -> None)

    let get_exn name attributes =
      match get name attributes with
      | Some value -> value
      | None ->
          raise
          @@ Failure
               ( Format.asprintf "required XML attributes %a missing"
                   Xmlm.pp_name name,
                 None )

    let optional name attributes = return @@ get name attributes

    let required name attributes =
      (* make sure the exception is bound within a Lwt promise *)
      return () >>= fun () -> return @@ get_exn name attributes
  end

  let parse_stream parser signals = parser signals
end

(* Tree *)

module Tree = struct
  type t =
    | Element of {
        name : Xmlm.name;
        attributes : Xmlm.attribute list;
        children : t list;
      }
    | Data of string

  let make_element ?(attributes = []) ?(children = []) name =
    Element { name; attributes; children }

  let make_data s = Data s

  let rec to_seq tree =
    match tree with
    | Data s -> Seq.return @@ `Data s
    | Element { name; attributes; children } ->
        Seq.cons
          (`El_start (name, attributes))
          Seq.(
            append (flat_map to_seq @@ List.to_seq children) (return `El_end))

  let rec pp ppf = function
    | Element { name; attributes; children } ->
        Fmt.pf ppf "@[<> @[<h><%a@ %a>@]@[<v>%a@]@[<h></%a>@]@]" Xmlm.pp_name
          name
          Fmt.(list ~sep:sp Xmlm.pp_attribute)
          attributes
          Fmt.(list ~sep:cut pp)
          children Xmlm.pp_name name
    | Data data -> Fmt.pf ppf "@[<v 0>%s@]" data

  let parser =
    Parser.(
      fix (fun m ->
          any_element (fun (name, attributes) ->
              (fun children -> make_element name ~attributes ~children)
              <$> many @@ choice [ (data >>| fun s -> Data s); m ])))

  let parse parser tree =
    tree |> to_seq |> Lwt_stream.of_seq |> Parser.parse_stream parser

  let parse_trees parser trees =
    trees |> Seq.flat_map to_seq |> Lwt_stream.of_seq
    |> Parser.parse_stream parser

  (* Misc *)

  let get_root_name = function
    | Element { name; _ } -> Some name
    | Data _ -> None

  let get_root_attribute name = function
    | Element { attributes; _ } -> Parser.Attributes.get name attributes
    | Data _ -> None
end

(* Namespace *)

module Namespace = struct
  let ns_xml = "http://www.w3.org/XML/1998/namespace"
  let ns_xmlns = "http://www.w3.org/2000/xmlns/"
  let xml local = (ns_xml, local)
  let xmlns local = (ns_xmlns, local)
  let default_xmlns_declaration ns = (xmlns "xmlns", ns)
end
