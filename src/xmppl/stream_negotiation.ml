(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

let src = Logs.Src.create "Xmpp.Stream_negotiation"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)
include Transport

module Make (Transport : TRANSPORT) = struct
  (* Helper to parser from Transport.stream *)
  let parse ?failure_parser parser stream =
    let parser =
      match failure_parser with
      | Some failure_parser ->
          Xmlc.Parser.(parser <|> (failure_parser >>= fail))
      | None -> parser
    in
    Xmlc.Parser.parse_stream parser (Transport.signals stream)

  module Sasl = Sasl.Make (Transport)

  module Feature = struct
    type t =
      | Sasl of Sasl.Mechanism.t list
      | ResourceBinding
      | Unknown of Xmlc.Tree.t

    let bind_parser =
      Xmlc.Parser.(
        element (Namespace.bind "bind") (fun _ ->
            option ignore_element >>| fun _ -> ResourceBinding))

    let parser =
      Xmlc.Parser.(
        element (Namespace.streams "features") (fun _ ->
            many
            @@ choice ~failure_msg:"invalid stream features"
                 [
                   bind_parser;
                   (Sasl.Mechanism.mechanisms_parser >>| fun m -> Sasl m);
                   (Xmlc.Tree.parser >>| fun tree -> Unknown tree);
                 ]))

    let pp ppf = function
      | Sasl mechanisms ->
          Fmt.pf ppf "Sasl (%a)" (Fmt.list Sasl.Mechanism.pp) mechanisms
      | ResourceBinding -> Fmt.pf ppf "Bind"
      | Unknown tree ->
          Fmt.pf ppf "Unknown (%a)" (Fmt.option Xmlm.pp_name)
            (Xmlc.Tree.get_root_name tree)
  end

  let bind_resource ?jid stream =
    let* () = Log.debug (fun m -> m "Binding resource.") in

    let* () =
      (Stanza.Iq.make_set ~id:"bind" ~to':None ?from:jid
      @@ Xmlc.Tree.(
           make_element
             ~attributes:
               [ Xmlc.Namespace.default_xmlns_declaration Namespace.bind_uri ]
             (Namespace.bind "bind")))
      |> Stanza.Iq.to_xml |> Transport.send_xml stream
    in

    let payload_parser =
      Xmlc.Parser.(
        element (Namespace.bind "bind") (fun _ ->
            element (Namespace.bind "jid") (fun _ ->
                data >>= fun jid_s ->
                match Jid.of_string jid_s with
                | Some jid -> return jid
                | None -> fail_with "invalid JID")))
    in

    let* result = parse Stanza.Iq.parser stream in

    match result with
    | Stanza.Iq.Result { id = "bind"; payload = Some payload; _ } ->
        payload |> Xmlc.Tree.parse payload_parser
    | _ -> fail_with "Resource binding failed"

  (* Perform Stream negotation.
   *
   * Returns the bound JID if stream negotiation is completed or None
   * if Stream needs to be restarted.  
   *)
  let negotiate ~credentials xml_stream =
    (* Parse the Stream features *)
    let* features =
      parse ~failure_parser:Stream.error_parser Feature.parser xml_stream
    in

    let* () =
      Log.debug (fun m ->
          m "Stream features: %a" (Fmt.list ~sep:Fmt.comma Feature.pp) features)
    in

    let sasl_mechansims =
      List.filter_map
        (function
          | Feature.Sasl sasl_mechanisms -> Some sasl_mechanisms | _ -> None)
        features
      |> List.flatten
    in

    if List.length sasl_mechansims > 0 then
      (* Do SASL negotiation *)
      let* sasl_result =
        Sasl.negotiate ~credentials sasl_mechansims xml_stream
      in
      match sasl_result with
      | Ok () ->
          let* () = Log.debug (fun m -> m "SASL negotation completed.") in
          let* () = Transport.stop_stream xml_stream in
          return_none
      | Error e ->
          let* () =
            Log.err (fun m ->
                m "SASL negotation failed: %s" (Printexc.to_string e))
          in
          let* () = Transport.stop_stream xml_stream in
          fail e
    else if List.mem Feature.ResourceBinding features then
      let jid =
        match credentials with
        | `JidPassword (jid, _password) -> Some jid
        | `Anonymous _ -> None
      in
      bind_resource ?jid xml_stream >|= Option.some
    else
      let* () = Log.err (fun m -> m "No usable features") in
      fail_with "No features I can use"
end
