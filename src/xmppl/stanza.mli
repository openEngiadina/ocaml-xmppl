(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type id = string

module Error : sig
  type error_type = Auth | Cancel | Continue | Modify | Wait

  type condition =
    | BadRequest
    | Conflict
    | FeatureNotImplemented
    | Forbidden
    | Gone
    | InternalServerError
    | ItemNotFound
    | JidMalformed
    | NotAcceptable
    | NotAllowed
    | NotAuthorized
    | PolicyViolation
    | RecipientUnavailable
    | Redirect
    | RegistrationRequired
    | RemoteServerNotFound
    | RemoteServerTimeout
    | ResourceConstraint
    | ServiceUnavailable
    | SubscriptionRequired
    | UndefinedCondition
    | UnexpectedRequest

  type t = error_type * condition * string option

  val pp : t Fmt.t
    [@@ocaml.toplevel_printer]
  (** [pp ppf t] will output a human readable representation of the
error to the formatter [ppf]. *)

  val to_string : t -> string
  val parser : t Xmlc.Parser.t
end

module Iq : sig
  type result = {
    id : id;
    to' : Jid.t option;
    from : Jid.t option;
    (* "An IQ stanza of type "result" MUST include zero or one child
       elements." (Section 8.2.3 of RFC6120) *)
    payload : Xmlc.Tree.t option;
  }

  val pp_result : result Fmt.t

  type error = {
    id : id;
    to' : Jid.t option;
    from : Jid.t option;
    error : Error.t;
  }

  val pp_error : error Fmt.t

  type get = {
    id : id;
    to' : Jid.t option;
    from : Jid.t option;
    (* "An IQ stanza of type "get" or "set" MUST contain exactly one
       child element, which specifies the semantics of the particular
       request." (Section 8.2.3 of RFC6120) *)
    payload : Xmlc.Tree.t;
  }

  val pp_get : get Fmt.t

  type set = {
    id : id;
    (* to' and from are string as sometimes server resources are addressed that are not JIDs *)
    to' : Jid.t option;
    from : Jid.t option;
    (* "An IQ stanza of type "get" or "set" MUST contain exactly one
       child element, which specifies the semantics of the particular
       request." (Section 8.2.3 of RFC6120) *)
    payload : Xmlc.Tree.t;
  }

  val pp_set : set Fmt.t

  type t = Get of get | Set of set | Result of result | Error of error

  val make_get : id:id -> to':Jid.t option -> ?from:Jid.t -> Xmlc.Tree.t -> t
  val make_set : id:id -> to':Jid.t option -> ?from:Jid.t -> Xmlc.Tree.t -> t

  val make_result :
    id:id -> ?to':Jid.t -> ?from:Jid.t -> Xmlc.Tree.t option -> t

  val to_xml : t -> Xmlc.Tree.t
  val parser : t Xmlc.Parser.t
  val pp : t Fmt.t
  val to_string : t -> string
end

module Message : sig
  type t = {
    id : id option;
    to' : Jid.t;
    from : Jid.t option;
    type' : string option;
    payload : Xmlc.Tree.t list;
  }

  val make :
    ?id:id -> to':Jid.t -> ?from:Jid.t -> ?type':string -> Xmlc.Tree.t list -> t

  val pp : t Fmt.t
  val to_xml : t -> Xmlc.Tree.t
  val parser : t Xmlc.Parser.t
end

module Presence : sig
  type t = {
    id : id option;
    to' : Jid.t option;
    from : Jid.t option;
    type' : string option;
    payload : Xmlc.Tree.t list;
  }

  val make :
    ?id:id ->
    ?to':Jid.t ->
    ?from:Jid.t ->
    ?type':string ->
    Xmlc.Tree.t list ->
    t

  val pp : t Fmt.t
  val to_xml : t -> Xmlc.Tree.t
  val parser : t Xmlc.Parser.t
end

type t = Message of Message.t | Presence of Presence.t | Iq of Iq.t

val parser : t Xmlc.Parser.t
val pp : t Fmt.t
