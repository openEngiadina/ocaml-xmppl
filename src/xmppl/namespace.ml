(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

let streams_uri = "http://etherx.jabber.org/streams"
let streams local = (streams_uri, local)
let ietf_streams_ns = "urn:ietf:params:xml:ns:xmpp-streams"
let ietf_streams local = (ietf_streams_ns, local)
let framing_uri = "urn:ietf:params:xml:ns:xmpp-framing"
let framing local = (framing_uri, local)
let client_uri = "jabber:client"
let client local = (client_uri, local)
let stanzas_uri = "urn:ietf:params:xml:ns:xmpp-stanzas"
let stanzas local = (stanzas_uri, local)
let bind_uri = "urn:ietf:params:xml:ns:xmpp-bind"
let bind local = (bind_uri, local)
let sasl_uri = "urn:ietf:params:xml:ns:xmpp-sasl"
let sasl local = (sasl_uri, local)
