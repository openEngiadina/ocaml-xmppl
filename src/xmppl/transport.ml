(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

module type TRANSPORT = sig
  type options
  type t

  val connect : host:string -> options -> t Lwt.t
  val close : t -> unit Lwt.t
  val closed : t -> unit Lwt.t

  type stream

  val open_stream : t -> to':string -> stream Lwt.t
  val stream_id : stream -> string Lwt.t
  val send_xml : stream -> Xmlc.Tree.t -> unit Lwt.t
  val signals : stream -> Xmlm.signal Lwt_stream.t
  val stop_stream : stream -> unit Lwt.t
end
