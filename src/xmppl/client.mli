(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** {1 XMPP Client} *)

(** {2 Transport} *)

module type TRANSPORT = sig
  (** A XMPP transport provides following functionalities:

- Resolve the address of the XMPP server given a JID
- Open and maintain connection to an XMPP server
- Start and stop XML Streams (as defined in RFC 6120)
- Expose an XML Stream as a stream of parsed XML signals

This level of abstraction allows the core XMPP client logic to be used
over a TCP socket on Unix and WebSockets on the web (using
`js_of_ocaml`).

*)

  (** {2 Connection} *)

  type options
  (** Additional options that may be passed to the transport *)

  type t
  (** Type of an instantiated connection to an XMPP server *)

  val connect : host:string -> options -> t Lwt.t
  val close : t -> unit Lwt.t
  val closed : t -> unit Lwt.t

  (** {2 XML Stream} *)

  type stream

  val open_stream : t -> to':string -> stream Lwt.t
  val stream_id : stream -> string Lwt.t
  val send_xml : stream -> Xmlc.Tree.t -> unit Lwt.t
  val signals : stream -> Xmlm.signal Lwt_stream.t
  val stop_stream : stream -> unit Lwt.t
end

(** {2 Client signature} *)

module type S = sig
  type transport
  (** {1 Transport} *)

  type transport_options

  (** {1 Client} *)

  type t
  (** Type of the client *)

  type credentials =
    [ `JidPassword of Jid.t * string  (** A JID password pair *)
    | `Anonymous of string
      (** Anonymously 
          @see <https://xmpp.org/extensions/xep-0175.html> XEP-0175: Best Practices or Use of SASL ANONYMOUS *)
    ]
  (** Credentials used to authenticate with XMPP server *)

  val create :
    ?seed:Random.State.t ->
    transport_options ->
    credentials:credentials ->
    t Lwt.t

  (** {1 Connection & State} *)

  val connect : t -> unit Lwt.t
  val disconnect : t -> unit Lwt.t

  type state = Disconnected | Connected of Jid.t

  val pp_state : state Fmt.t
  val state : t -> state React.signal
  val on_connect : t -> Jid.t React.event
  val jid : t -> Jid.t Lwt.t
  (* Returns a promise that resolves to the current JID if connected
     or to a pending promise that will resolve once the client is
     connected. *)

  (** {1 XMPP} *)

  val stanzas : t -> Stanza.t React.event
  val iq_gets : ?element:Xmlm.name -> t -> Stanza.Iq.get React.event
  val iq_sets : ?element:Xmlm.name -> t -> Stanza.Iq.set React.event

  exception StanzaError of Stanza.Error.t

  val iq_get : t -> ?to':Jid.t -> Xmlc.Tree.t -> Stanza.Iq.result Lwt.t
  val iq_set : t -> ?to':Jid.t -> Xmlc.Tree.t -> Stanza.Iq.result Lwt.t
  val send_message : t -> Stanza.Message.t -> unit Lwt.t
  val send_iq : t -> Stanza.Iq.t -> unit Lwt.t
  val send_presence : t -> Stanza.Presence.t -> unit Lwt.t

  (** {1 Low-levels} *)

  val send_xml : t -> Xmlc.Tree.t -> unit Lwt.t
  val generate_id : t -> string
end

(** Constructor for a XMPP client *)
module Make (Transport : TRANSPORT) :
  S
    with type transport = Transport.t
     and type transport_options = Transport.options
