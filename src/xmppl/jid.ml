(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type t = { local : string option; domain : string; resource : string option }

let make local domain resource = { local; domain; resource }
let is_bare jid = match jid.resource with None -> true | _ -> false
let bare jid = { jid with resource = None }

let parser =
  let open Angstrom in
  choice
    [
      make
      <$> (take_till (function '@' -> true | _ -> false) >>| Option.some)
      <* char '@'
      <*> take_till (function '/' -> true | _ -> false)
      <*> option None (char '/' *> take_while1 (fun _ -> true) >>| Option.some);
      (* No local part *)
      make None
      <$> take_till (function '/' -> true | _ -> false)
      <*> option None (char '/' *> take_while1 (fun _ -> true) >>| Option.some);
    ]

let of_string s =
  Angstrom.(parse_string ~consume:Consume.All parser s) |> Result.to_option

let of_string_exn s =
  match of_string s with Some s -> s | None -> failwith "Invalid JID"

let pp ppf jid =
  match (jid.local, jid.resource) with
  | None, None -> Fmt.pf ppf "@[%s@]" jid.domain
  | None, Some resource -> Fmt.pf ppf "@[%s/%s@]" jid.domain resource
  | Some local, None -> Fmt.pf ppf "@[%s@%s@]" local jid.domain
  | Some local, Some resource ->
      Fmt.pf ppf "@[%s@%s/%s@]" local jid.domain resource

let to_string = Fmt.to_to_string pp
let compare a b = String.compare (to_string a) (to_string b)

type jid = t

module Map = Map.Make (struct
  type t = jid

  let compare = compare
end)
