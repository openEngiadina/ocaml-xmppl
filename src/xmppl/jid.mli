(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type t = { local : string option; domain : string; resource : string option }

val make : string option -> string -> string option -> t
val is_bare : t -> bool
val bare : t -> t
val of_string : string -> t option
val of_string_exn : string -> t
val pp : t Fmt.t [@@ocaml.toplevel_printer]
val to_string : t -> string
val compare : t -> t -> int

module Map : Map.S with type key = t
