(*
 * SPDX-FileCopyrightText: 2022 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type condition =
  | BadFormat
  | BadNamespacePrefix
  | Conflict
  | ConnectionTimeout
  | HostGone
  | HostUnknown
  | ImproperAddressing
  | InternalServerError
  | InvalidFrom
  | InvalidNamespace
  | InvalidXml
  | NotAuthorized
  | NotWellFormed
  | PolicyViolation
  | RemoteConnectionFailed
  | Reset
  | ResourceConstraint
  | RestrictedXml
  | SeeOtherHost
  | SystemShutdown
  | UndefinedCondition
  | UnsupportedEncoding
  | UnsupportedFeature
  | UnsupportedStanzaType
  | UnsupportedVersion

let condition_to_string = function
  | BadFormat -> "bad-format"
  | BadNamespacePrefix -> "bad-namespace-prefix"
  | Conflict -> "conflict"
  | ConnectionTimeout -> "connection-timeout"
  | HostGone -> "host-gone"
  | HostUnknown -> "host-unknown"
  | ImproperAddressing -> "improper-addressing"
  | InternalServerError -> "internal-server-error"
  | InvalidFrom -> "invalid-form"
  | InvalidNamespace -> "invalid-namespace"
  | InvalidXml -> "invalid-xml"
  | NotAuthorized -> "not-authorized"
  | NotWellFormed -> "not-well-formed"
  | PolicyViolation -> "policy-violation"
  | RemoteConnectionFailed -> "remote-connection-failed"
  | Reset -> "reset"
  | ResourceConstraint -> "resource-contraint"
  | RestrictedXml -> "restricted-xml"
  | SeeOtherHost -> "see-other-host"
  | SystemShutdown -> "system-shutdown"
  | UndefinedCondition -> "undefined-condition"
  | UnsupportedEncoding -> "unsupported-encoding"
  | UnsupportedFeature -> "unsupported-feature"
  | UnsupportedStanzaType -> "unsupported-stanza-type"
  | UnsupportedVersion -> "unsupported-version"

exception Error of condition * string option

let error_parser =
  let text_parser =
    Xmlc.Parser.(element (Namespace.ietf_streams "text") (fun _ -> data))
  in

  let make_condition_parser n e =
    Xmlc.Parser.(element (Namespace.ietf_streams n) (fun _ -> return e))
  in

  let condition_parser =
    Xmlc.Parser.(
      choice ~failure_msg:"Invalid Stream error condition"
        [
          make_condition_parser "bad-format" BadFormat;
          make_condition_parser "bad-namespace-prefix" BadNamespacePrefix;
          make_condition_parser "conflict" Conflict;
          make_condition_parser "connection-timeout" ConnectionTimeout;
          make_condition_parser "host-gone" HostGone;
          make_condition_parser "host-unknown" HostUnknown;
          make_condition_parser "improper-addressing" ImproperAddressing;
          make_condition_parser "internal-server-error" InternalServerError;
          make_condition_parser "invalid-from" InvalidFrom;
          make_condition_parser "invalid-namespace" InvalidNamespace;
          make_condition_parser "invalid-xml" InvalidXml;
          make_condition_parser "not-authorized" NotAuthorized;
          make_condition_parser "not-well-formed" NotWellFormed;
          make_condition_parser "policy-violation" PolicyViolation;
          make_condition_parser "remote-connection-failed"
            RemoteConnectionFailed;
          make_condition_parser "reset" Reset;
          make_condition_parser "resource-constraint" ResourceConstraint;
          make_condition_parser "restricted-xml" RestrictedXml;
          make_condition_parser "see-other-host" SeeOtherHost;
          make_condition_parser "system-shutdown" SystemShutdown;
          make_condition_parser "undefined-condition" UndefinedCondition;
          make_condition_parser "unsupported-encoding" UnsupportedEncoding;
          make_condition_parser "unsupported-feature" UnsupportedFeature;
          make_condition_parser "unsupported-stanza-type" UnsupportedStanzaType;
          make_condition_parser "unsupported-version" UnsupportedVersion;
        ])
  in

  Xmlc.Parser.(
    element (Namespace.streams "error") (fun _attributes ->
        (fun condition description -> Error (condition, description))
        <$> condition_parser <*> option text_parser <* many ignore_any))

let () =
  Printexc.register_printer (function
    | Error (condition, None) ->
        Some (Printf.sprintf "StreamError(%s)" (condition_to_string condition))
    | Error (condition, Some description) ->
        Some
          (Printf.sprintf "SaslFailure(%s, %s)"
             (condition_to_string condition)
             description)
    | _ -> None)
