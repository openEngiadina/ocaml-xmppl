(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax

let src = Logs.Src.create "Xmpp.Sasl"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)
include Transport

(* Define the error type in a separate module so that it can be exposed *)

module Sasl_error = struct
  type error =
    | Aborted
    | AccountDisabled
    | CredentialsExpired
    | EncryptionRequired
    | IncorrectEncoding
    | InvalidAuthzid
    | InvalidMechanism
    | MalformedRequest
    | MechanismTooWeak
    | NotAuthorized
    | TemporaryAuthFailure

  let error_to_string = function
    | Aborted -> "aborted"
    | AccountDisabled -> "account-disabled"
    | CredentialsExpired -> "credentials-expired"
    | EncryptionRequired -> "encryption-required"
    | IncorrectEncoding -> "incorrect-encoding"
    | InvalidAuthzid -> "invalid-authzid"
    | InvalidMechanism -> "invalid-mechansim"
    | MalformedRequest -> "malformed-request"
    | MechanismTooWeak -> "mechanism-too-weak"
    | NotAuthorized -> "not-authorized"
    | TemporaryAuthFailure -> "temporary-auth-failure"

  exception SaslFailure of error * string option

  let () =
    Printexc.register_printer (function
      | SaslFailure (t, None) ->
          Some (Printf.sprintf "SaslFailure(%s)" (error_to_string t))
      | SaslFailure (t, Some msg) ->
          Some (Printf.sprintf "SaslFailure(%s, %s)" (error_to_string t) msg)
      | _ -> None)
end

module Make (Transport : TRANSPORT) = struct
  include Sasl_error

  (* Helper to parser from Transport.stream *)
  let parse ?failure_parser parser stream =
    let parser =
      match failure_parser with
      | Some failure_parser ->
          Xmlc.Parser.(parser <|> (failure_parser >>= fail))
      | None -> parser
    in
    Xmlc.Parser.parse_stream parser (Transport.signals stream)

  let failure_parser =
    let text_parser =
      Xmlc.Parser.(element (Namespace.sasl "text") (fun _ -> data))
    in

    let make_error_parser n e =
      Xmlc.Parser.(element (Namespace.sasl n) (fun _ -> return e))
    in

    let error_parser =
      Xmlc.Parser.(
        choice ~failure_msg:"unknown SASL failure"
          [
            make_error_parser "aborted" Aborted;
            make_error_parser "credentials-expired" CredentialsExpired;
            make_error_parser "encryption-required" EncryptionRequired;
            make_error_parser "incorrect-encoding" IncorrectEncoding;
            make_error_parser "invalid-authzid" InvalidAuthzid;
            make_error_parser "invalid-mechanism" InvalidMechanism;
            make_error_parser "malformed-request" MalformedRequest;
            make_error_parser "mechanism-too-weak" MechanismTooWeak;
            make_error_parser "not-authorized" NotAuthorized;
            make_error_parser "temporary-auth-failure" TemporaryAuthFailure;
          ])
    in

    Xmlc.Parser.(
      element (Namespace.sasl "failure") (fun _ ->
          (fun error text -> SaslFailure (error, text))
          <$> error_parser <*> option text_parser))

  module Mechanism = struct
    type t = PLAIN | SCRAM_SHA_1 | ANONYMOUS | Not_supported of string

    let parser =
      Xmlc.Parser.(
        element (Namespace.sasl "mechanism") (fun _ ->
            data >>= function
            | "PLAIN" -> return PLAIN
            | "SCRAM-SHA-1" -> return SCRAM_SHA_1
            | "ANONYMOUS" -> return ANONYMOUS
            | id -> return @@ Not_supported id))

    let mechanisms_parser =
      Xmlc.Parser.(element (Namespace.sasl "mechanisms") (fun _ -> many parser))

    let to_string = function
      | PLAIN -> "PLAIN"
      | SCRAM_SHA_1 -> "SCRAM_SHA_1"
      | ANONYMOUS -> "ANONYMOUS"
      | Not_supported s -> s

    let pp = Fmt.of_to_string to_string
  end

  let negotiate_scram_sha_1 ~(jid : Jid.t) ~password xml_stream =
    let module SCRAM = Digestif_scram.Make (Digestif.SHA1) in
    let* () = Log.debug (fun m -> m "Stating SASL SCRAM-SHA-1 negotiation") in

    let* username =
      match jid.local with
      | Some username -> return username
      | None -> fail_invalid_arg "No local part in JID"
    in

    (* Send initial message *)
    let client_initial_message = SCRAM.client_initial_message ~username in
    let* () =
      Transport.send_xml xml_stream
        Xmlc.Tree.(
          make_element
            ~attributes:
              [
                (* TODO do not use namespace for attributes *)
                (Namespace.sasl "mechanism", "SCRAM-SHA-1");
                Xmlc.Namespace.default_xmlns_declaration Namespace.sasl_uri;
              ]
            ~children:
              [
                make_data
                @@ SCRAM.encode_client_initial_message client_initial_message;
              ]
            (Namespace.sasl "auth"))
    in

    (* Read the server challenge *)
    let challenge_parser =
      Xmlc.Parser.(
        element (Namespace.sasl "challenge") (fun _ ->
            data >>= fun challenge_encoded ->
            match SCRAM.decode_server_challenge challenge_encoded with
            | Ok v -> return v
            | Error msg -> fail_with msg))
    in

    let* server_challenge = parse ~failure_parser challenge_parser xml_stream in

    (* Send client repsonse *)
    let* client_response =
      match
        SCRAM.client_response ~password client_initial_message server_challenge
      with
      | Ok cr -> return cr
      | Error msg -> fail_with msg
    in

    let* () =
      Transport.send_xml xml_stream
        Xmlc.Tree.(
          make_element
            ~attributes:
              [ Xmlc.Namespace.default_xmlns_declaration Namespace.sasl_uri ]
            ~children:
              [ make_data @@ SCRAM.encode_client_response client_response ]
            (Namespace.sasl "response"))
    in

    (* Read server response *)
    let server_response_parser =
      Xmlc.Parser.(
        element (Namespace.sasl "success") (fun _ ->
            data >>= fun server_signature ->
            match
              SCRAM.check_server_response client_response server_signature
            with
            | Ok () -> return ()
            | Error msg -> fail_with msg))
    in

    parse ~failure_parser server_response_parser xml_stream

  let negotiate_plain ~(jid : Jid.t) ~password xml_stream =
    let* () = Log.debug (fun m -> m "Stating SASL PLAIN negotiation") in

    let null = Char.chr 0 |> Seq.return |> String.of_seq in

    let* username =
      match jid.local with
      | Some username -> return username
      | None -> fail_invalid_arg "No local part in JID"
    in

    let message = null ^ username ^ null ^ password in

    let* () =
      Transport.send_xml xml_stream
        Xmlc.Tree.(
          make_element
            ~attributes:
              [
                Xmlc.Namespace.default_xmlns_declaration Namespace.sasl_uri;
                (* TODO do not use namespace for attributes *)
                (Namespace.sasl "mechanism", "PLAIN");
              ]
            ~children:[ make_data @@ Base64.encode_string message ]
            (Namespace.sasl "auth"))
    in

    let success_parser =
      Xmlc.Parser.(element (Namespace.sasl "success") (fun _ -> return ()))
    in

    parse ~failure_parser success_parser xml_stream

  let negotiate_anonymous xml_stream =
    let* () = Log.debug (fun m -> m "Stating SASL ANONYMOUS negotiation") in
    let* () =
      Transport.send_xml xml_stream
        Xmlc.Tree.(
          make_element
            ~attributes:
              [
                Xmlc.Namespace.default_xmlns_declaration Namespace.sasl_uri;
                (* TODO do not use namespace for attributes *)
                (Namespace.sasl "mechanism", "ANONYMOUS");
              ]
            (Namespace.sasl "auth"))
    in
    let success_parser =
      Xmlc.Parser.(element (Namespace.sasl "success") (fun _ -> return ()))
    in
    parse ~failure_parser success_parser xml_stream

  let negotiate ~credentials mechanisms xml_stream =
    match credentials with
    | `JidPassword (jid, password) ->
        if List.mem Mechanism.SCRAM_SHA_1 mechanisms then
          Lwt_result.catch @@ negotiate_scram_sha_1 ~jid ~password xml_stream
        else if List.mem Mechanism.PLAIN mechanisms then
          Lwt_result.catch @@ negotiate_plain ~jid ~password xml_stream
        else return_error (Failure "no supported SASL mechanism")
    | `Anonymous _host_name ->
        if List.mem Mechanism.ANONYMOUS mechanisms then
          Lwt_result.catch @@ negotiate_anonymous xml_stream
        else
          return_error
            (Failure "SASL mechanism ANONYMOUS is not supported by host")
end
