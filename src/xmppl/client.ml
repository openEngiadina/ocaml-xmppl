(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

open Lwt
open Lwt.Syntax
open Lwt_react

let src = Logs.Src.create "Xmppl.Client"

module Log = (val Logs_lwt.src_log src : Logs_lwt.LOG)
include Transport

module type S = sig
  type transport
  type transport_options
  type t
  type credentials = [ `JidPassword of Jid.t * string | `Anonymous of string ]

  val create :
    ?seed:Random.State.t ->
    transport_options ->
    credentials:credentials ->
    t Lwt.t

  val connect : t -> unit Lwt.t
  val disconnect : t -> unit Lwt.t

  type state = Disconnected | Connected of Jid.t

  val pp_state : state Fmt.t
  val state : t -> state React.signal
  val on_connect : t -> Jid.t React.event
  val jid : t -> Jid.t Lwt.t
  val stanzas : t -> Stanza.t React.event
  val iq_gets : ?element:Xmlm.name -> t -> Stanza.Iq.get React.event
  val iq_sets : ?element:Xmlm.name -> t -> Stanza.Iq.set React.event

  exception StanzaError of Stanza.Error.t

  val iq_get : t -> ?to':Jid.t -> Xmlc.Tree.t -> Stanza.Iq.result Lwt.t
  val iq_set : t -> ?to':Jid.t -> Xmlc.Tree.t -> Stanza.Iq.result Lwt.t
  val send_message : t -> Stanza.Message.t -> unit Lwt.t
  val send_iq : t -> Stanza.Iq.t -> unit Lwt.t
  val send_presence : t -> Stanza.Presence.t -> unit Lwt.t
  val send_xml : t -> Xmlc.Tree.t -> unit Lwt.t
  val generate_id : t -> string
end

module Make (Transport : TRANSPORT) = struct
  (* Helper to parser from Transport.stream *)
  let parse ?failure_parser parser stream =
    let parser =
      match failure_parser with
      | Some failure_parser ->
          Xmlc.Parser.(parser <|> (failure_parser >>= fail))
      | None -> parser
    in
    Xmlc.Parser.parse_stream parser (Transport.signals stream)

  (* Stream negotiation  *)
  module Stream_negotiation = Stream_negotiation.Make (Transport)
  module Sasl = Stream_negotiation.Sasl

  type transport = Transport.t
  type transport_options = Transport.options

  type transport_state = {
    transport : Transport.t;
    stream : Transport.stream;
    reader : unit Lwt.t;
  }

  type state_internal = IDisconnected | IConnected of Jid.t * transport_state
  type credentials = [ `JidPassword of Jid.t * string | `Anonymous of string ]

  let pp_credentials ppf = function
    | `JidPassword (jid, _) -> Fmt.pf ppf "%a" Jid.pp jid
    | `Anonymous host_name -> Fmt.pf ppf "ANONYMOUS (%s)" host_name

  type t = {
    (* Random seed used for ID generation *)
    seed : Random.State.t;
    (* Credentials used to authenticate *)
    credentials : credentials;
    (* Transport options *)
    transport_options : transport_options;
    (* State *)
    state_s : state_internal React.signal;
    state_update : ?step:React.step -> state_internal -> unit;
    (* Stanza events. These event persists over reconnects *)
    stanza_e : Stanza.t React.event;
    stanza_push : ?step:React.step -> Stanza.t -> unit;
  }

  let istate client = S.value client.state_s

  let create ?(seed = Random.State.make_self_init ()) transport_options
      ~credentials =
    let state_s, state_update = S.create IDisconnected in
    let stanza_e, stanza_push = E.create () in
    return
      {
        seed;
        credentials;
        transport_options;
        state_s;
        state_update;
        stanza_e;
        stanza_push;
      }

  type state = Disconnected | Connected of Jid.t

  let pp_state ppf = function
    | Disconnected -> Fmt.pf ppf "Disconnected"
    | Connected jid -> Fmt.pf ppf "Connected (%a)" Jid.pp jid

  let state client =
    client.state_s
    |> S.map (function
         | IDisconnected -> Disconnected
         | IConnected (jid, _) -> Connected jid)

  let on_connect client =
    client.state_s |> S.changes
    |> E.map (function
         | IDisconnected -> None
         | IConnected (jid, _) -> Some jid)
    |> E.filter Option.is_some |> E.map Option.get

  let jid client =
    match istate client with
    | IDisconnected -> on_connect client |> E.next
    | IConnected (jid, _) -> return jid

  let stanzas t = t.stanza_e

  let iq_gets ?element t =
    stanzas t
    |> E.map (function
         | Stanza.(
             Iq (Iq.Get ({ payload = Xmlc.Tree.Element { name; _ }; _ } as get)))
           ->
             let element_matches =
               Option.map (fun element -> name = element) element
               |> Option.value ~default:true
             in
             if element_matches then Some get else None
         | _ -> None)
    |> E.filter Option.is_some |> E.map Option.get

  let iq_sets ?element t =
    stanzas t
    |> E.map (function
         | Stanza.(
             Iq (Iq.Set ({ payload = Xmlc.Tree.Element { name; _ }; _ } as set)))
           ->
             let element_matches =
               Option.map (fun element -> name = element) element
               |> Option.value ~default:true
             in
             if element_matches then Some set else None
         | _ -> None)
    |> E.filter Option.is_some |> E.map Option.get

  let send_xml client xml =
    match istate client with
    | IConnected (_jid, transport) ->
        let* () = Log.debug (fun m -> m "SEND:\n%a" Xmlc.Tree.pp xml) in
        Transport.send_xml transport.stream xml
    | _ -> fail_with "not connected"

  let generate_id client = Uuidm.v4_gen client.seed () |> Uuidm.to_string

  exception StanzaError of Stanza.Error.t

  let () =
    Printexc.register_printer (function
      | StanzaError e -> Some (Stanza.Error.to_string e)
      | _ -> None)

  let iq_response client id' =
    (* filter incoming stanzas for response to iq *)
    let* response =
      stanzas client
      |> E.fmap (fun stanza ->
             match stanza with
             | Stanza.Iq (Stanza.Iq.Result ({ id; _ } as result)) when id = id'
               ->
                 Some (Ok result)
             | Stanza.Iq (Stanza.Iq.Error ({ id; _ } as error)) when id = id' ->
                 Some (Error error)
             | _ -> None)
      |> E.once |> E.next
    in
    match response with
    | Ok response -> return response
    | Error e -> fail (StanzaError e.error)

  let iq_get client ?to' payload =
    let* jid = jid client in
    let id = generate_id client in

    (* Promise that resolves when IQ response is received *)
    let response = iq_response client id in

    (* Send IQ *)
    let* () =
      Stanza.Iq.make_get ~id ~to' ~from:jid payload
      |> Stanza.Iq.to_xml |> send_xml client
    in

    response

  let iq_set client ?to' payload =
    let* jid = jid client in
    let id = generate_id client in

    (* Promise that resolves when IQ response is received *)
    let response = iq_response client id in

    (* Send IQ *)
    let* () =
      Stanza.Iq.make_set ~id ~to' ~from:jid payload
      |> Stanza.Iq.to_xml |> send_xml client
    in

    response

  let send_message client message =
    message |> Stanza.Message.to_xml |> send_xml client

  let send_iq client iq = iq |> Stanza.Iq.to_xml |> send_xml client

  let send_presence client presence =
    presence |> Stanza.Presence.to_xml |> send_xml client

  let rec negotiate ~(credentials : credentials) transport =
    let* () = Log.debug (fun m -> m "Starting XMPP stream negotiation.") in

    let to' =
      match credentials with
      | `JidPassword (jid, _) -> jid.domain
      | `Anonymous host_name -> host_name
    in

    (* Create an XML stream from the transport *)
    let* xml_stream_res =
      Lwt_result.catch @@ Transport.open_stream ~to' transport
    in

    let* xml_stream =
      match xml_stream_res with
      | Ok xml_stream -> return xml_stream
      | Error exn ->
          Log.err (fun m ->
              m "Opening an XML Stream failed: %s" (Printexc.to_string exn))
          >>= fun _ -> fail exn
    in

    (* Negotiate stream *)
    let* negotiated_stream =
      Stream_negotiation.negotiate ~credentials xml_stream
    in

    match negotiated_stream with
    | Some jid ->
        let* () = Log.debug (fun m -> m "Stream negotiation done.") in
        return (jid, xml_stream)
    | None ->
        let* () = Log.debug (fun m -> m "Restarting XML stream.") in
        negotiate ~credentials transport

  let start_stanza_reader client xml_stream =
    (* parses stanzas from the xml stream. Returns only when strem
       ends or an error is encountered. *)

    (* all the things we expect in the XML Stream *)
    let stream_element_parser =
      Xmlc.Parser.(
        Stanza.parser
        >>| (fun s -> Some s)
        (* Xmlm emits end elements for all opened elements *)
        <|> (el_end >>| fun _ -> None)
        (* When using framing (WebSocket) the stream just ends *)
        <|> (stream_end >>| fun _ -> None))
    in

    (* the read loop *)
    let rec read () =
      let* stream_element =
        Lwt_result.catch
        @@ parse ~failure_parser:Stream.error_parser stream_element_parser
             xml_stream
      in
      match stream_element with
      | Ok (Some stanza) ->
          let* () = Log.debug (fun m -> m "RECV STANZA: %a" Stanza.pp stanza) in
          client.stanza_push stanza;
          read ()
      | Ok None -> return_ok ()
      | Error exn -> return_error exn
    in

    read ()

  let connect client =
    (* make sure we are not already connected *)
    let* () =
      match S.value client.state_s with
      | IConnected _ -> fail @@ Invalid_argument "client is already connected"
      | IDisconnected -> return_unit
    in

    let* () =
      Log.debug (fun m ->
          m "Creating client connection for %a" pp_credentials
            client.credentials)
    in

    let host =
      match client.credentials with
      | `JidPassword (jid, _) -> jid.domain
      | `Anonymous host -> host
    in

    (* Create a transport *)
    let* transport =
      Lwt_result.catch @@ Transport.connect ~host client.transport_options
      >>= function
      | Ok transport -> return transport
      | Error exn ->
          Log.err (fun m ->
              m "Creating a transport failed: %s" (Printexc.to_string exn))
          >>= fun _ -> fail exn
    in

    (* Stream negotation (without resource binding) *)
    let* bound_jid, stream =
      negotiate ~credentials:client.credentials transport
    in

    (* Start reading stanzas from the stream *)
    let reader =
      Lwt.catch
        (fun () ->
          start_stanza_reader client stream >>= function
          | Ok () ->
              Log.info (fun m -> m "XMPP Stream ended. Closing transport.")
              >>= fun () ->
              Transport.close transport >|= fun () ->
              client.state_update @@ IDisconnected
          | Error err ->
              Log.warn (fun m ->
                  m "Error on XMPP Stream (%s). Closing transport."
                    (Printexc.to_string err))
              >>= fun () ->
              Transport.close transport >|= fun () ->
              client.state_update @@ IDisconnected)
        (fun e ->
          Log.err (fun m ->
              m "Stanza reader failure: %s" @@ Printexc.to_string e)
          >>= fun () ->
          Transport.close transport >|= fun () ->
          client.state_update @@ IDisconnected)
    in

    (* Update state *)
    client.state_update @@ IConnected (bound_jid, { transport; stream; reader });

    let* () =
      Log.info (fun m -> m "XMPP Client connected as %a." Jid.pp bound_jid)
    in

    return_unit

  let disconnect client =
    match istate client with
    | IConnected (_, transport) ->
        (* TODO gracefully disconnect by setting presence to unavailable and closing stream. *)
        let* () = Transport.stop_stream transport.stream in
        let* () = Log.info (fun m -> m "XMPP Client disconnected.") in
        return @@ client.state_update IDisconnected
    | _ -> return_unit
end
