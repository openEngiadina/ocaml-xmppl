(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

type id = string

module Error = struct
  type error_type = Auth | Cancel | Continue | Modify | Wait

  let error_type_to_string = function
    | Auth -> "auth"
    | Cancel -> "cancel"
    | Continue -> "continue"
    | Modify -> "modify"
    | Wait -> "wait"

  let error_type_of_string = function
    | "auth" -> Auth
    | "cancel" -> Cancel
    | "continue" -> Continue
    | "modify" -> Modify
    | "wait" -> Wait
    | _ -> failwith "Invalid Stanza Error type"

  type condition =
    | BadRequest
    | Conflict
    | FeatureNotImplemented
    | Forbidden
    | Gone
    | InternalServerError
    | ItemNotFound
    | JidMalformed
    | NotAcceptable
    | NotAllowed
    | NotAuthorized
    | PolicyViolation
    | RecipientUnavailable
    | Redirect
    | RegistrationRequired
    | RemoteServerNotFound
    | RemoteServerTimeout
    | ResourceConstraint
    | ServiceUnavailable
    | SubscriptionRequired
    | UndefinedCondition
    | UnexpectedRequest

  let condition_to_string = function
    | BadRequest -> "bad-request"
    | Conflict -> "conflict"
    | FeatureNotImplemented -> "feature-not-implemented"
    | Forbidden -> "forbidden"
    | Gone -> "gone"
    | InternalServerError -> "internal-server-error"
    | ItemNotFound -> "item-not-found"
    | JidMalformed -> "jid-malformed"
    | NotAcceptable -> "not-acceptable"
    | NotAllowed -> "not-allowed"
    | NotAuthorized -> "not-authorized"
    | PolicyViolation -> "policy-violation"
    | RecipientUnavailable -> "recipient-unavailable"
    | Redirect -> "redirect"
    | RegistrationRequired -> "registration-required"
    | RemoteServerNotFound -> "remote-server-not-found"
    | RemoteServerTimeout -> "remote-server-timeout"
    | ResourceConstraint -> "resource-contraint"
    | ServiceUnavailable -> "service-unavailable"
    | SubscriptionRequired -> "subscription-required"
    | UndefinedCondition -> "undefined-condition"
    | UnexpectedRequest -> "unexpected-request"

  type t = error_type * condition * string option

  let to_string = function
    | t, condition, None ->
        Printf.sprintf "StanzaError(%s, %s)" (error_type_to_string t)
          (condition_to_string condition)
    | t, condition, Some msg ->
        Printf.sprintf "StanzaError(%s, %s, %s)" (error_type_to_string t)
          (condition_to_string condition)
          msg

  let pp = Fmt.of_to_string to_string

  let parser =
    let text_parser =
      Xmlc.Parser.(element (Namespace.stanzas "text") (fun _ -> data))
    in

    let make_condition_parser n e =
      Xmlc.Parser.(element (Namespace.stanzas n) (fun _ -> return e))
    in

    let condition_parser =
      Xmlc.Parser.(
        choice ~failure_msg:"Unknown Stanza Error condtion"
          [
            make_condition_parser "bad-request" BadRequest;
            make_condition_parser "conflict" Conflict;
            make_condition_parser "feature-not-implemented"
              FeatureNotImplemented;
            make_condition_parser "forbidden" Forbidden;
            make_condition_parser "gone" Gone;
            make_condition_parser "internal-server-error" InternalServerError;
            make_condition_parser "item-not-found" ItemNotFound;
            make_condition_parser "jid-malformed" JidMalformed;
            make_condition_parser "not-acceptable" NotAcceptable;
            make_condition_parser "not-authorized" NotAuthorized;
            make_condition_parser "policy-violation" PolicyViolation;
            make_condition_parser "recipient-unavailable" RecipientUnavailable;
            make_condition_parser "redirect" Redirect;
            make_condition_parser "registration-required" RegistrationRequired;
            make_condition_parser "remote-server-not-found" RemoteServerNotFound;
            make_condition_parser "remote-server-timeout" RemoteServerTimeout;
            make_condition_parser "resource-constraint" ResourceConstraint;
            make_condition_parser "service-unavailable" ServiceUnavailable;
            make_condition_parser "subscription-required" SubscriptionRequired;
            make_condition_parser "undefined-condtion" UndefinedCondition;
            make_condition_parser "unexpected-request" UnexpectedRequest;
          ])
    in

    Xmlc.Parser.(
      element (Namespace.client "error") (fun attributes ->
          (fun type' condition text -> (type', condition, text))
          <$> (Attributes.required ("", "type") attributes
              >>| error_type_of_string)
          <*> condition_parser <*> option text_parser))
end

module Iq = struct
  type result = {
    id : id;
    to' : Jid.t option;
    from : Jid.t option;
    (* "An IQ stanza of type "result" MUST include zero or one child
       elements." (Section 8.2.3 of RFC6120) *)
    payload : Xmlc.Tree.t option;
  }

  let pp_result =
    Fmt.record
      [
        Fmt.field "id" (fun r -> r.id) Fmt.string;
        Fmt.field "to"
          (fun r -> r.to')
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "from"
          (fun r -> r.from)
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "payload"
          (fun r -> r.payload)
          Fmt.(option ~none:(styled `Faint @@ any "None") Xmlc.Tree.pp);
      ]

  type error = {
    id : id;
    to' : Jid.t option;
    from : Jid.t option;
    error : Error.t; (* payload : Xmlc.t option; *)
  }

  let pp_error =
    Fmt.record
      [
        Fmt.field "id" (fun e -> e.id) Fmt.string;
        Fmt.field "to"
          (fun r -> r.to')
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "from"
          (fun r -> r.from)
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "error" (fun e -> e.error) Error.pp;
      ]

  type get = {
    id : id;
    to' : Jid.t option;
    from : Jid.t option;
    (* "An IQ stanza of type "get" or "set" MUST contain exactly one
       child element, which specifies the semantics of the particular
       request." (Section 8.2.3 of RFC6120) *)
    payload : Xmlc.Tree.t;
  }

  let pp_get =
    Fmt.record
      [
        Fmt.field "id" (fun r -> r.id) Fmt.string;
        Fmt.field "to"
          (fun r -> r.to')
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "from"
          (fun r -> r.from)
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "payload" (fun r -> r.payload) Xmlc.Tree.pp;
      ]

  type set = {
    id : id;
    to' : Jid.t option;
    from : Jid.t option;
    (* "An IQ stanza of type "get" or "set" MUST contain exactly one
       child element, which specifies the semantics of the particular
       request." (Section 8.2.3 of RFC6120) *)
    payload : Xmlc.Tree.t;
  }

  let pp_set =
    Fmt.record
      [
        Fmt.field "id" (fun r -> r.id) Fmt.string;
        Fmt.field "to"
          (fun r -> r.to')
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "from"
          (fun r -> r.from)
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "payload" (fun r -> r.payload) Xmlc.Tree.pp;
      ]

  type t = Get of get | Set of set | Result of result | Error of error

  let make_get ~id ~to' ?from payload = Get { id; to'; from; payload }
  let make_set ~id ~to' ?from payload = Set { id; to'; from; payload }
  let make_result ~id ?to' ?from payload = Result { id; to'; from; payload }

  let to_xml = function
    | Get iq ->
        Xmlc.Tree.make_element
          ~attributes:
            List.(
              filter_map
                (fun x -> x)
                [
                  Some
                    (Xmlc.Namespace.default_xmlns_declaration
                       Namespace.client_uri);
                  Some (Namespace.client "id", iq.id);
                  Some (Namespace.client "type", "get");
                  Option.map
                    (fun from -> (Namespace.client "from", Jid.to_string from))
                    iq.from;
                  Option.map
                    (fun to' -> (Namespace.client "to", Jid.to_string to'))
                    iq.to';
                ])
          ~children:[ iq.payload ]
        @@ Namespace.client "iq"
    | Set iq ->
        Xmlc.Tree.make_element
          ~attributes:
            List.(
              filter_map
                (fun x -> x)
                [
                  Some
                    (Xmlc.Namespace.default_xmlns_declaration
                       Namespace.client_uri);
                  Some (Namespace.client "id", iq.id);
                  Some (Namespace.client "type", "set");
                  Option.map
                    (fun from -> (Namespace.client "from", Jid.to_string from))
                    iq.from;
                  Option.map
                    (fun to' -> (Namespace.client "to", Jid.to_string to'))
                    iq.to';
                ])
          ~children:[ iq.payload ]
        @@ Namespace.client "iq"
    | Result iq ->
        Xmlc.Tree.make_element
          ~attributes:
            List.(
              filter_map
                (fun x -> x)
                [
                  Some
                    (Xmlc.Namespace.default_xmlns_declaration
                       Namespace.client_uri);
                  Some (Namespace.client "id", iq.id);
                  Some (Namespace.client "type", "result");
                  Option.map
                    (fun from -> (Namespace.client "from", Jid.to_string from))
                    iq.from;
                  Option.map
                    (fun to' -> (Namespace.client "to", Jid.to_string to'))
                    iq.to';
                ])
          ~children:List.(filter_map (fun x -> x) [ iq.payload ])
        @@ Namespace.client "iq"
    | Error iq ->
        Xmlc.Tree.make_element
          ~attributes:
            List.(
              filter_map
                (fun x -> x)
                [
                  Some
                    (Xmlc.Namespace.default_xmlns_declaration
                       Namespace.client_uri);
                  Some (Namespace.client "id", iq.id);
                  Some (Namespace.client "type", "error");
                  Option.map
                    (fun from -> (Namespace.client "from", Jid.to_string from))
                    iq.from;
                  Option.map
                    (fun to' -> (Namespace.client "to", Jid.to_string to'))
                    iq.to';
                ])
          ~children:(failwith "TODO: Implement Error.to_xml")
        @@ Namespace.client "iq"

  let parser =
    Xmlc.Parser.(
      element (Namespace.client "iq") (fun attributes ->
          let id = Attributes.get_exn ("", "id") attributes in

          let from =
            Attributes.get ("", "from") attributes
            |> Option.map Jid.of_string_exn
          in
          let to' =
            Attributes.get ("", "to") attributes |> Option.map Jid.of_string_exn
          in

          match Attributes.get ("", "type") attributes with
          | Some "get" ->
              (fun payload -> Get { id; to'; from; payload })
              <$> Xmlc.Tree.parser
          | Some "set" ->
              (fun payload -> Set { id; to'; from; payload })
              <$> Xmlc.Tree.parser
          | Some "result" ->
              (fun payload -> Result { id; to'; from; payload })
              <$> option Xmlc.Tree.parser
          | Some "error" ->
              fix (fun p ->
                  (fun error -> Error { id; to'; from; error })
                  <$> Error.parser <* many ignore_any
                  (* This is to handle the optionally added XML that was sent that caused the error *)
                  <|> ignore_any *> p)
          | Some _ -> fail_with "invalid IQ stanza type"
          | None -> fail_with "no type in IQ stanza"))

  let pp ppf = function
    | Get iq -> Fmt.pf ppf "@[Get %a@]" pp_get iq
    | Set iq -> Fmt.pf ppf "@[Set %a@]" pp_set iq
    | Result iq -> Fmt.pf ppf "@[Result %a@]" pp_result iq
    | Error error -> Fmt.pf ppf "@[Error %a@]" pp_error error

  let to_string iq = Fmt.to_to_string pp iq
end

module Message = struct
  type t = {
    id : id option;
    to' : Jid.t;
    from : Jid.t option;
    type' : string option;
    payload : Xmlc.Tree.t list;
  }

  let make ?id ~to' ?from ?type' payload = { id; to'; from; type'; payload }

  let pp =
    Fmt.record
      [
        Fmt.field "id"
          (fun r -> r.id)
          Fmt.(option ~none:(styled `Faint @@ any "None") string);
        Fmt.field "to" (fun r -> r.to') Jid.pp;
        Fmt.field "from"
          (fun r -> r.from)
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "type"
          (fun r -> r.id)
          Fmt.(option ~none:(styled `Faint @@ any "None") string);
        Fmt.field "payload" (fun r -> r.payload) Fmt.(list Xmlc.Tree.pp);
      ]

  let to_xml message =
    Xmlc.Tree.make_element
      ~attributes:
        List.(
          filter_map
            (fun x -> x)
            [
              (* Add the XML Namespace here for WebSocket framing. *)
              Some
                (Xmlc.Namespace.default_xmlns_declaration Namespace.client_uri);
              Option.map (fun id -> (Namespace.client "id", id)) message.id;
              Some (Namespace.client "to", Jid.to_string message.to');
              Option.map
                (fun from -> (Namespace.client "from", Jid.to_string from))
                message.from;
              Option.map
                (fun type' -> (Namespace.client "type", type'))
                message.type';
            ])
      ~children:message.payload
    @@ Namespace.client "message"

  let parser =
    Xmlc.Parser.(
      element (Namespace.client "message") (fun attributes ->
          let id = Attributes.get ("", "id") attributes in

          let from =
            Attributes.get ("", "from") attributes
            |> Option.map Jid.of_string_exn
          in
          let to' =
            Attributes.get_exn ("", "to") attributes
            |> Jid.of_string |> Option.get
          in
          let type' = Attributes.get ("", "type") attributes in

          (fun payload -> { id; to'; from; type'; payload })
          <$> many Xmlc.Tree.parser))
end

module Presence = struct
  type t = {
    id : id option;
    to' : Jid.t option;
    from : Jid.t option;
    type' : string option;
    payload : Xmlc.Tree.t list;
  }

  let make ?id ?to' ?from ?type' payload = { id; to'; from; type'; payload }

  let pp =
    Fmt.record
      [
        Fmt.field "id"
          (fun r -> r.id)
          Fmt.(option ~none:(styled `Faint @@ any "None") string);
        Fmt.field "to"
          (fun r -> r.to')
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "from"
          (fun r -> r.from)
          Fmt.(option ~none:(styled `Faint @@ any "None") Jid.pp);
        Fmt.field "type"
          (fun r -> r.type')
          Fmt.(option ~none:(styled `Faint @@ any "None") string);
        Fmt.field "payload" (fun r -> r.payload) (Fmt.list Xmlc.Tree.pp);
      ]

  let to_xml p =
    Xmlc.Tree.make_element
      ~attributes:
        List.(
          filter_map
            (fun x -> x)
            [
              (* Add the XML Namespace here for WebSocket framing. *)
              Some
                (Xmlc.Namespace.default_xmlns_declaration Namespace.client_uri);
              Option.map (fun id -> (Namespace.client "id", id)) p.id;
              Option.map
                (fun to' -> (Namespace.client "to", Jid.to_string to'))
                p.to';
              Option.map
                (fun from -> (Namespace.client "from", Jid.to_string from))
                p.from;
              Option.map (fun type' -> (Namespace.client "type", type')) p.type';
            ])
      ~children:p.payload
    @@ Namespace.client "presence"

  let parser =
    Xmlc.Parser.(
      element (Namespace.client "presence") (fun attributes ->
          let id = Attributes.get ("", "id") attributes in

          let from =
            Option.bind (Attributes.get ("", "from") attributes) Jid.of_string
          in
          let to' =
            Option.bind (Attributes.get ("", "to") attributes) Jid.of_string
          in
          let type' = Attributes.get ("", "type") attributes in

          (fun payload -> { id; to'; from; type'; payload })
          <$> many Xmlc.Tree.parser))
end

type t = Message of Message.t | Presence of Presence.t | Iq of Iq.t

let parser =
  Xmlc.Parser.(
    choice
      [
        (Iq.parser >>| fun iq -> Iq iq);
        (Presence.parser >>| fun p -> Presence p);
        (Message.parser >>| fun msg -> Message msg);
      ])

let pp ppf = function
  | Message msg -> Fmt.pf ppf "@[Message %a@]" Message.pp msg
  | Presence p -> Fmt.pf ppf "@[Presense %a@]" Presence.pp p
  | Iq iq -> Fmt.pf ppf "@[IQ %a@]" Iq.pp iq
