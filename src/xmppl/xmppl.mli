(*
 * SPDX-FileCopyrightText: 2021 pukkamustard <pukkamustard@posteo.net>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 *)

(** A reactive XMPP client. *)

(** {2 SASL} *)

module Sasl : sig
  type error =
    | Aborted
    | AccountDisabled
    | CredentialsExpired
    | EncryptionRequired
    | IncorrectEncoding
    | InvalidAuthzid
    | InvalidMechanism
    | MalformedRequest
    | MechanismTooWeak
    | NotAuthorized
    | TemporaryAuthFailure

  exception SaslFailure of error * string option
end

(** {2 XMPP} *)

module Stream : sig
  type condition =
    | BadFormat
    | BadNamespacePrefix
    | Conflict
    | ConnectionTimeout
    | HostGone
    | HostUnknown
    | ImproperAddressing
    | InternalServerError
    | InvalidFrom
    | InvalidNamespace
    | InvalidXml
    | NotAuthorized
    | NotWellFormed
    | PolicyViolation
    | RemoteConnectionFailed
    | Reset
    | ResourceConstraint
    | RestrictedXml
    | SeeOtherHost
    | SystemShutdown
    | UndefinedCondition
    | UnsupportedEncoding
    | UnsupportedFeature
    | UnsupportedStanzaType
    | UnsupportedVersion

  exception Error of condition * string option
end

module Jid = Jid
module Stanza = Stanza
module Client = Client
module Namespace = Namespace
