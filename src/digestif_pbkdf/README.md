# ocaml-digestif-pbkdf

This is an adaptation of [ocaml-pbkdf](https://github.com/abeaumont/ocaml-pbkdf) to use [Digestif](https://github.com/mirage/digestif) instead of [mirage-crypto](https://github.com/mirage/mirage-crypto). This allows the usage of the Pbkdf functionality on platforms that mirage-crypto does not support (e.g. with js_of_ocaml).
